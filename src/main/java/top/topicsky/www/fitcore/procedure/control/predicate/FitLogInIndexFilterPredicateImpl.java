package top.topicsky.www.fitcore.procedure.control.predicate;

import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import top.topicsky.www.fitcore.global.dto.FitCoreLogInBeanDto;
import top.topicsky.www.fitcore.global.dto.FitCoreLogInFormDto;
import top.topicsky.www.fitcore.global.resource.constant.FitCoreStaticFinalLayout;
import top.topicsky.www.fitcore.procedure.control.process.FitLogInServletProcessImpl;
import top.topicsky.www.fitcore.procedure.service.predicate.FitLogInServicePredicateImpl;
import top.topicsky.www.fitcore.procedure.service.process.FitLogInServiceProcessImpl;

import java.io.Serializable;
import java.util.Arrays;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.procedure.control.predicate.system.login
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 12 日 10 时 49 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Controller
@Transactional
public class FitLogInIndexFilterPredicateImpl implements Serializable{
    /**
     * Get filter boolean.
     *
     * @param fitCoreLogInBeanDto
     *         the fit core log in bean
     *
     * @return the boolean
     */
    public static boolean getFilter(FitCoreLogInBeanDto fitCoreLogInBeanDto){
        return Arrays.asList(fitCoreLogInBeanDto)
                .stream()
                .allMatch(fitCoreLogInBeanMatch->
                        fitCoreLogInBeanMatch.getCookies().length>0
                                &&fitCoreLogInBeanMatch.getCookies()!=null
                );
    }

    /**
     * Get bean filter dispatcher string.
     *
     * @param fitCoreLogInBeanDto
     *         the fit core log in bean
     *
     * @return the string
     */
    public static String getBeanFilterDispatcher(FitCoreLogInBeanDto fitCoreLogInBeanDto){
        try{
            if(FitLogInServiceProcessImpl.fitLogInServiceProcessLogin(fitCoreLogInBeanDto,FitLogInServicePredicateImpl::getProcessLogin)){
                FitLogInServletProcessImpl.fitLogInServletProcessService(fitCoreLogInBeanDto,FitLogInServletPredicateImpl::getCheck);
                return FitCoreStaticFinalLayout.INDEXJSP;
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return FitCoreStaticFinalLayout.LOGINJSP;
    }

    /**
     * Get bean from cookie.
     *
     * @param fitCoreLogInBeanDto
     *         the fit core log in bean
     */
    public static void getBeanFromCookie(FitCoreLogInBeanDto fitCoreLogInBeanDto){
        if(fitCoreLogInBeanDto.getStrings().length==2){
            FitCoreLogInFormDto fitCoreLogInFormDto=new FitCoreLogInFormDto();
            fitCoreLogInFormDto.setUsername(fitCoreLogInBeanDto.getStrings()[0]);
            fitCoreLogInFormDto.setPassword(fitCoreLogInBeanDto.getStrings()[1]);
            fitCoreLogInBeanDto.setFitCoreLogInFormDto(fitCoreLogInFormDto);
        }
    }
}
