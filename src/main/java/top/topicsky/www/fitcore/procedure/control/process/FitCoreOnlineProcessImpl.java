package top.topicsky.www.fitcore.procedure.control.process;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.context.support.WebApplicationContextUtils;
import top.topicsky.www.fitcore.global.dto.FitCoreOnlineBeanDto;
import top.topicsky.www.fitcore.global.dto.FitCoreOnlineFormDto;
import top.topicsky.www.fitcore.global.exception.impl.FitCoreIllegalStateExceptionAdapterImpl;
import top.topicsky.www.fitcore.procedure.service.inter.FitCoreOnlineServiceInter;
import top.topicsky.www.fitcore.system.kernel.engine.Engine_Basic_Object_Inter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.Serializable;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.procedure.control.predicate
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 19 日 11 时 20 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Controller
@Transactional
public class FitCoreOnlineProcessImpl implements Serializable{
    /**
     * Fit core online process init string.
     *
     * @param initString
     *         the init string
     * @param initPredicate
     *         the init predicate
     *
     * @return the string
     */
    public static String FitCoreOnlineProcessInit(String initString,Engine_Basic_Object_Inter<String,String> initPredicate){
        /**
         * 转发初始化处理过程控制
         * */
        return initPredicate.fitCoreEngine(initString);
    }

    /**
     * Fit core online process init nodel string.
     *
     * @param initString
     *         the init string
     * @param model
     *         the model
     * @param initPredicate
     *         the init predicate
     *
     * @return the string
     */
    public static String FitCoreOnlineProcessInitNodel(String initString,Model model,Engine_Basic_Object_Inter<Model,Boolean> initPredicate){
        /**
         * 页面元素初始化处理过程
         * */
        if(initPredicate.fitCoreEngine(model)){
            return initString;
        }else{
            return "error/page_500";
        }
    }

    /**
     * Fit core online process init process fit core online bean dto.
     *
     * @param fitCoreOnlineFormDto
     *         the fit core online form dto
     * @param httpServletRequest
     *         the http servlet request
     * @param httpServletResponse
     *         the http servlet response
     * @param initPredicate
     *         the init predicate
     *
     * @return the fit core online bean dto
     */
    public static FitCoreOnlineBeanDto FitCoreOnlineProcessInitProcess(FitCoreOnlineFormDto fitCoreOnlineFormDto,
                                                                       HttpServletRequest httpServletRequest,
                                                                       HttpServletResponse httpServletResponse,
                                                                       Engine_Basic_Object_Inter<FitCoreOnlineFormDto,Boolean> initPredicate){
        BeanFactory beans=WebApplicationContextUtils.getWebApplicationContext(httpServletRequest.getSession().getServletContext());
        FitCoreOnlineServiceInter fitCoreOnlineServiceInter=(FitCoreOnlineServiceInter)beans.getBean("fitCoreOnlineServiceImpl");
        FitCoreOnlineBeanDto fitCoreOnlineBeanDto=new FitCoreOnlineBeanDto();
        fitCoreOnlineBeanDto.setRequest(httpServletRequest);
        fitCoreOnlineBeanDto.setResponse(httpServletResponse);
        fitCoreOnlineBeanDto.setFitCoreOnlineFormDto(fitCoreOnlineFormDto);
        fitCoreOnlineBeanDto.setFitCoreOnlineServiceInterInit(fitCoreOnlineServiceInter);
        if(initPredicate.fitCoreEngine(fitCoreOnlineFormDto)){
            return fitCoreOnlineBeanDto;
        }else{
            throw new FitCoreIllegalStateExceptionAdapterImpl("登陆执行状态错误！");
        }
    }

    /**
     * Fit core online process init process check.
     *
     * @param fitCoreOnlineFormDto
     *         the fit core online form dto
     * @param Process
     *         the process
     * @param model
     *         the model
     * @param fitCoreOnlineBeanDto
     *         the fit core online bean dto
     */
    public static void FitCoreOnlineProcessInitProcessCheck(FitCoreOnlineFormDto fitCoreOnlineFormDto,
                                                            String Process,
                                                            Model model,
                                                            FitCoreOnlineBeanDto fitCoreOnlineBeanDto){
        //fitCoreLogInBeanDto.getRequest().getRequestDispatcher(
        //        FitLogInServletProcessImpl.fitLogInServletProcessgetBeanDispatcher(
        //                fitCoreLogInBeanDto,
        //                FitLogInServletPredicateImpl::getBeanDispatcher)
        //).forward(fitCoreLogInBeanDto.getRequest(),fitCoreLogInBeanDto.getResponse());
    }
}
