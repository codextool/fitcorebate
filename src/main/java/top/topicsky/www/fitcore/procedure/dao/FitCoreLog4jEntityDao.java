package top.topicsky.www.fitcore.procedure.dao;

import org.apache.ibatis.annotations.CacheNamespace;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import top.topicsky.www.fitcore.global.entity.FitCoreLog4jEntity;

import java.io.Serializable;
import java.util.List;

/**
 * date : 2017-06-07 17:28:20
 * author : mapper generator
 * description : FitCoreLog4jEntityDao数据库操作接口类
 */
@Repository
@Transactional
@CacheNamespace(size=512)
public interface FitCoreLog4jEntityDao extends Serializable{
    /**
     * 根据Id查询记录
     *
     * @param fitCore_stamp
     *         the fit core stamp
     *
     * @return the fit core log 4 j entity
     */
    FitCoreLog4jEntity selectById(@Param("fitCore_stamp") Long fitCore_stamp);

    /**
     * 根据给定的记录查询一批记录
     *
     * @param fitCore_stamps
     *         the fit core stamps
     *
     * @return the list
     */
    List<FitCoreLog4jEntity> selectByIds(@Param("fitCore_stamps") List<Long> fitCore_stamps);

    /**
     * 新增记录
     *
     * @param pojo
     *         the pojo
     *
     * @return the int
     */
    int insert(@Param("pojo") FitCoreLog4jEntity pojo);

    /**
     * 新增记录，只匹配有值的字段
     *
     * @param pojo
     *         the pojo
     *
     * @return the int
     */
    int insertSelective(@Param("pojo") FitCoreLog4jEntity pojo);

    /**
     * 根据Id更新记录
     *
     * @param pojo
     *         the pojo
     *
     * @return the int
     */
    int updateById(@Param("pojo") FitCoreLog4jEntity pojo);

    /**
     * 根据Id更新记录,只更新有值的字段
     *
     * @param pojo
     *         the pojo
     *
     * @return the int
     */
    int updateSelectiveById(@Param("pojo") FitCoreLog4jEntity pojo);

    /**
     * 根据Id删除记录
     *
     * @param fitCore_stamp
     *         the fit core stamp
     *
     * @return the int
     */
    int deleteById(@Param("fitCore_stamp") Long fitCore_stamp);

    /**
     * 根据Id删除一批记录
     *
     * @param fitCore_stamps
     *         the fit core stamps
     *
     * @return the int
     */
    int deleteByIds(@Param("fitCore_stamps") List<Long> fitCore_stamps);
}
