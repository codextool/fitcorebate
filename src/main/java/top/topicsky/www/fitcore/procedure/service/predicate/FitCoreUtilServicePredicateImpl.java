package top.topicsky.www.fitcore.procedure.service.predicate;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import top.topicsky.www.fitcore.global.dto.FitCoreLogInBeanDto;
import top.topicsky.www.fitcore.global.entity.FitCoreUserEntity;
import top.topicsky.www.fitcore.procedure.service.process.FitCoreUtilServiceProcessImpl;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.procedure.service.process.system.login
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 12 日 17 时 40 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Service
@Transactional
public class FitCoreUtilServicePredicateImpl implements Serializable{
    /**
     * Generate excel view map.
     *
     * @param fitCoreLogInBeanDto
     *         the fit core log in bean dto
     *
     * @return the map
     */
    public static Map<String,String> generateExcelView(FitCoreLogInBeanDto fitCoreLogInBeanDto){
        FitCoreUserEntity fitCoreUserEntityTemp=new FitCoreUserEntity();
        fitCoreUserEntityTemp.setFitCoreCname(fitCoreLogInBeanDto.getFitCoreLogInFormDto().getUsername());
        FitCoreUserEntity fitCoreUserEntity=fitCoreLogInBeanDto.getFitCoreUserServiceImplInit().selectLogIn(fitCoreUserEntityTemp);
        return FitCoreUtilServiceProcessImpl.generateExcelViewSource(fitCoreUserEntity,FitCoreUtilServicePredicateImpl::generateExcelViewSource);
    }

    /**
     * Generate excel view source map.
     *
     * @param fitCoreUserEntity
     *         the fit core user entity
     *
     * @return the map
     */
    public static Map<String,String> generateExcelViewSource(FitCoreUserEntity fitCoreUserEntity){
        Map<String,String> returnMap=new HashMap<>();
        returnMap.put(fitCoreUserEntity.getFitCoreCname(),fitCoreUserEntity.getFitCorePassword());
        return returnMap;
    }
}
