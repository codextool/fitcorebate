package top.topicsky.www.fitcore.procedure.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import top.topicsky.www.fitcore.global.entity.MycodeEntity;
import top.topicsky.www.fitcore.procedure.dao.MycodeEntityDao;

import java.io.Serializable;
import java.util.List;

/**
 * The type Mycode entity service.
 */
@Service
@Transactional
public class MycodeEntityServiceImpl implements MycodeEntityServiceInter, Serializable{
    @Qualifier("mycodeEntityDao")
    @Autowired
    private MycodeEntityDao mycodeEntityDao;

    public int insert(MycodeEntity pojo){
        return mycodeEntityDao.insert(pojo);
    }

    public int insertSelective(MycodeEntity pojo){
        return mycodeEntityDao.insertSelective(pojo);
    }

    public int insertList(List<MycodeEntity> pojos){
        return mycodeEntityDao.insertList(pojos);
    }

    public int update(MycodeEntity pojo){
        return mycodeEntityDao.update(pojo);
    }
}
