package top.topicsky.www.fitcore.procedure.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import top.topicsky.www.fitcore.global.entity.FitCoreUserEntity;
import top.topicsky.www.fitcore.procedure.dao.FitCoreUserEntityDao;
import top.topicsky.www.fitcore.procedure.service.inter.FitCoreUserServiceInter;

import java.io.Serializable;
import java.util.List;

/**
 * The type Fit core user service.
 */
@Service
@Transactional
public class FitCoreOnlineServiceImpl implements FitCoreUserServiceInter, Serializable{
    @Qualifier("fitCoreUserEntityDao")
    @Autowired
    private FitCoreUserEntityDao fitCoreUserEntityDao;

    @Override
    public FitCoreUserEntity selectLogIn(FitCoreUserEntity pojo){
        return fitCoreUserEntityDao.selectLogIn(pojo);
    }

    public int insert(FitCoreUserEntity pojo){
        return fitCoreUserEntityDao.insert(pojo);
    }

    public int insertSelective(FitCoreUserEntity pojo){
        return fitCoreUserEntityDao.insertSelective(pojo);
    }

    public int insertList(List<FitCoreUserEntity> pojos){
        return fitCoreUserEntityDao.insertList(pojos);
    }

    public int update(FitCoreUserEntity pojo){
        return fitCoreUserEntityDao.update(pojo);
    }
}
