package top.topicsky.www.fitcore.procedure.control.process;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.support.WebApplicationContextUtils;
import top.topicsky.www.fitcore.global.dto.FitCoreLogInBeanDto;
import top.topicsky.www.fitcore.global.util.predicate.plugs.FitCoreCookieURLEncoderPredicateImpl;
import top.topicsky.www.fitcore.global.util.process.plugs.FitCoreCookieURLEncoderProcessImpl;
import top.topicsky.www.fitcore.procedure.control.predicate.FitLogInIndexFilterPredicateImpl;
import top.topicsky.www.fitcore.procedure.service.inter.FitLogInServiceInter;
import top.topicsky.www.fitcore.system.kernel.engine.Engine_Basic_NoReturn_Inter;
import top.topicsky.www.fitcore.system.kernel.engine.Engine_Basic_Object_Inter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Serializable;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.procedure.control.process.system.login
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 12 日 10 时 40 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Controller
@Transactional
public class FitLogInIndexFilterProcessImpl implements Serializable{
    /**
     * Fit log in index filter process get filter.
     *
     * @param fitCoreLogInBeanDto
     *         the fit core log in bean
     */
    public static void fitLogInIndexFilterProcessGetFilter(FitCoreLogInBeanDto fitCoreLogInBeanDto){
        try{
            fitCoreLogInBeanDto.getRequest().getRequestDispatcher(
                    FitLogInIndexFilterProcessImpl.fitLogInIndexFilterProcessGetFilterDispatcher(
                            fitCoreLogInBeanDto,
                            FitLogInIndexFilterPredicateImpl::getBeanFilterDispatcher)
            ).forward(fitCoreLogInBeanDto.getRequest(),fitCoreLogInBeanDto.getResponse());
        }catch(ServletException e){
            e.printStackTrace();
        }catch(IOException e){
            e.printStackTrace();
        }
    }

    /**
     * Fit log in index filter process get filter dispatcher fit core log in bean.
     *
     * @param fitCoreLogInBeanDto
     *         the fit core log in bean
     * @param predicateDispatcher
     *         the predicate dispatcher
     *
     * @return the fit core log in bean
     */
    public static String fitLogInIndexFilterProcessGetFilterDispatcher(FitCoreLogInBeanDto fitCoreLogInBeanDto,Engine_Basic_Object_Inter<FitCoreLogInBeanDto,String> predicateDispatcher){
        return predicateDispatcher.fitCoreEngine(fitCoreLogInBeanDto);
    }

    /**
     * Fit log in index filter process get filter init fit core log in bean.
     *
     * @param arg0
     *         the arg 0
     * @param arg1
     *         the arg 1
     * @param arg2
     *         the arg 2
     * @param predicateInit
     *         the predicate init
     *
     * @return the fit core log in bean
     */
    public static FitCoreLogInBeanDto fitLogInIndexFilterProcessGetFilterInit(ServletRequest arg0,ServletResponse arg1,FilterChain arg2,Engine_Basic_Object_Inter<FitCoreLogInBeanDto,Boolean> predicateInit){
        HttpServletRequest request=(HttpServletRequest)arg0;
        HttpServletResponse response=(HttpServletResponse)arg1;
        BeanFactory beans=WebApplicationContextUtils.getWebApplicationContext(request.getSession().getServletContext());
        FitLogInServiceInter fitLogInServiceInter=(FitLogInServiceInter)beans.getBean("fitLogInServiceImpl");
        FitCoreLogInBeanDto fitCoreLogInBeanDto=new FitCoreLogInBeanDto();
        fitCoreLogInBeanDto.setRequest(request);
        fitCoreLogInBeanDto.setResponse(response);
        fitCoreLogInBeanDto.setCookies(request.getCookies());
        fitCoreLogInBeanDto.setFitLogInServiceImplInit(fitLogInServiceInter);
        Cookie[] cookies=FitCoreCookieURLEncoderProcessImpl.FitCoreCookieURLDecoderProcess(
                fitCoreLogInBeanDto.getCookies(),
                "GBK",
                FitCoreCookieURLEncoderPredicateImpl::cookieDecoder);
        Runnable runnable=()->FitLogInIndexFilterProcessImpl.fitLogInIndexFilterProcessGetFilterCookieToBean(
                fitCoreLogInBeanDto,
                FitLogInIndexFilterPredicateImpl::getBeanFromCookie);
        if(predicateInit.fitCoreEngine(fitCoreLogInBeanDto)){
            for(Cookie coo : cookies){
                fitCoreLogInBeanDto.setStrings(coo.getValue().split("=="));
                runnable.run();
            }
        }
        return fitCoreLogInBeanDto;
    }

    /**
     * Fit log in index filter process get filter cookie to bean.
     *
     * @param fitCoreLogInBeanDto
     *         the fit core log in bean
     * @param predicateCookieToBean
     *         the predicate cookie to bean
     */
    public static void fitLogInIndexFilterProcessGetFilterCookieToBean(FitCoreLogInBeanDto fitCoreLogInBeanDto,Engine_Basic_NoReturn_Inter<FitCoreLogInBeanDto> predicateCookieToBean){
        predicateCookieToBean.fitCoreEngine(fitCoreLogInBeanDto);
    }
}
