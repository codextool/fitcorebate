package top.topicsky.www.fitcore.procedure.service.predicate;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import top.topicsky.www.fitcore.global.dto.FitCoreLogInBeanDto;
import top.topicsky.www.fitcore.global.entity.FitCoreUserEntity;
import top.topicsky.www.fitcore.procedure.service.process.FitLogInServiceProcessImpl;

import java.io.Serializable;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.procedure.service.process.system.login
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 12 日 17 时 40 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Service
@Transactional
public class FitLogInServicePredicateImpl implements Serializable{
    /**
     * Get process check boolean.
     *
     * @param fitCoreLogInBeanDto
     *         the fit core log in bean
     *
     * @return the boolean
     */
    public static boolean getProcessCheck(FitCoreLogInBeanDto fitCoreLogInBeanDto){
        boolean bl=false;
        FitCoreUserEntity fitCoreUserEntityTemp=new FitCoreUserEntity();
        fitCoreUserEntityTemp.setFitCoreCname(fitCoreLogInBeanDto.getFitCoreLogInFormDto().getUsername());
        Boolean booleanCheck=FitLogInServiceProcessImpl.fitCoreLogInFormCheck(fitCoreLogInBeanDto,
                (FitCoreLogInBeanDto fitCoreLogInBeanLambdaForm)->fitCoreLogInBeanLambdaForm.getFitCoreLogInFormDto()!=null);
        if(booleanCheck){
            FitCoreUserEntity fitCoreUserEntity=fitCoreLogInBeanDto.getFitCoreUserServiceImplInit().selectLogIn(fitCoreUserEntityTemp);
            fitCoreLogInBeanDto.setFitCoreUserEntity(fitCoreUserEntity);
            Boolean booleanFormCheck=FitLogInServiceProcessImpl.fitCoreLogInFormCheck(fitCoreLogInBeanDto,
                    (FitCoreLogInBeanDto fitCoreLogInBeanLambdaEntity)->fitCoreLogInBeanLambdaEntity.getFitCoreUserEntity()!=null);
            if(booleanFormCheck){
                if(fitCoreUserEntity.getFitCorePassword().equals(fitCoreLogInBeanDto.getFitCoreLogInFormDto().getPassword())){
                    bl=true;
                }
            }
        }
        return bl;
    }

    /**
     * Get process login boolean.
     *
     * @param fitCoreLogInBeanDto
     *         the fit core log in bean
     *
     * @return the boolean
     */
    public static boolean getProcessLogin(FitCoreLogInBeanDto fitCoreLogInBeanDto){
        return fitCoreLogInBeanDto.getFitLogInServiceImplInit().login(fitCoreLogInBeanDto);
    }
}
