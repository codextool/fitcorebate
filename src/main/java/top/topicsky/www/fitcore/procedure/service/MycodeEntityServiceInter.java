package top.topicsky.www.fitcore.procedure.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import top.topicsky.www.fitcore.global.entity.MycodeEntity;

import java.io.Serializable;
import java.util.List;

/**
 * The interface Mycode entity service inter.
 */
@Service
@Transactional
public interface MycodeEntityServiceInter extends Serializable{
    /**
     * Insert int.
     *
     * @param pojo
     *         the pojo
     *
     * @return the int
     */
    public int insert(MycodeEntity pojo);

    /**
     * Insert selective int.
     *
     * @param pojo
     *         the pojo
     *
     * @return the int
     */
    public int insertSelective(MycodeEntity pojo);

    /**
     * Insert list int.
     *
     * @param pojos
     *         the pojos
     *
     * @return the int
     */
    public int insertList(List<MycodeEntity> pojos);

    /**
     * Update int.
     *
     * @param pojo
     *         the pojo
     *
     * @return the int
     */
    public int update(MycodeEntity pojo);
}
