package top.topicsky.www.fitcore.procedure.control.process;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.support.WebApplicationContextUtils;
import top.topicsky.www.fitcore.global.dto.FitCoreLogInBeanDto;
import top.topicsky.www.fitcore.global.dto.FitCoreLogInFormDto;
import top.topicsky.www.fitcore.global.resource.constant.FitCoreStaticFinalLayout;
import top.topicsky.www.fitcore.global.util.predicate.plugs.FitCoreCookieURLEncoderPredicateImpl;
import top.topicsky.www.fitcore.global.util.process.plugs.FitCoreCookieURLEncoderProcessImpl;
import top.topicsky.www.fitcore.procedure.control.predicate.FitLogInServletPredicateImpl;
import top.topicsky.www.fitcore.procedure.service.inter.FitLogInServiceInter;
import top.topicsky.www.fitcore.system.kernel.engine.Engine_Basic_Object_Inter;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Serializable;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.procedure.control.process.system.login
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 12 日 10 时 40 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Controller
@Transactional
public class FitLogInServletProcessImpl implements Serializable{
    /**
     * Fit log in servlet process get check.
     *
     * @param fitCoreLogInBeanDto
     *         the fit core log in bean
     */
    public static void fitLogInServletProcessGetCheck(FitCoreLogInBeanDto fitCoreLogInBeanDto){
        try{
            fitCoreLogInBeanDto.getRequest().getRequestDispatcher(
                    FitLogInServletProcessImpl.fitLogInServletProcessgetBeanDispatcher(
                            fitCoreLogInBeanDto,
                            FitLogInServletPredicateImpl::getBeanDispatcher)
            ).forward(fitCoreLogInBeanDto.getRequest(),fitCoreLogInBeanDto.getResponse());
        }catch(ServletException e){
            e.printStackTrace();
        }catch(IOException e){
            e.printStackTrace();
        }
    }

    /**
     * Fit log in servlet processget bean dispatcher string.
     *
     * @param fitCoreLogInBeanDto
     *         the fit core log in bean
     * @param predicateControlDispatcher
     *         the predicate control dispatcher
     *
     * @return the string
     */
    public static String fitLogInServletProcessgetBeanDispatcher(FitCoreLogInBeanDto fitCoreLogInBeanDto,Engine_Basic_Object_Inter<FitCoreLogInBeanDto,String> predicateControlDispatcher){
        return predicateControlDispatcher.fitCoreEngine(fitCoreLogInBeanDto);
    }

    /**
     * Fit log in servlet process get bean init fit core log in bean.
     *
     * @param request
     *         the request
     * @param response
     *         the response
     * @param predicateControlInit
     *         the predicate control init
     *
     * @return the fit core log in bean
     */
    public static FitCoreLogInBeanDto fitLogInServletProcessGetBeanInit(HttpServletRequest request,
                                                                        HttpServletResponse response,
                                                                        Engine_Basic_Object_Inter<FitCoreLogInBeanDto,Boolean> predicateControlInit){
        BeanFactory beans=WebApplicationContextUtils.getWebApplicationContext(request.getSession().getServletContext());
        FitLogInServiceInter fitLogInServiceImplInit=(FitLogInServiceInter)beans.getBean("fitLogInServiceImpl");
        FitCoreLogInBeanDto fitCoreLogInBeanDto=new FitCoreLogInBeanDto();
        FitCoreLogInFormDto fitCoreLogInFormDto=new FitCoreLogInFormDto();
        fitCoreLogInFormDto.setUsername(request.getParameter("username"));
        fitCoreLogInFormDto.setPassword(request.getParameter("password"));
        fitCoreLogInFormDto.setSavetime(request.getParameter("saveTime"));
        fitCoreLogInBeanDto.setRequest(request);
        fitCoreLogInBeanDto.setResponse(response);
        fitCoreLogInBeanDto.setFitCoreLogInFormDto(fitCoreLogInFormDto);
        fitCoreLogInBeanDto.setFitLogInServiceImplInit(fitLogInServiceImplInit);
        if(predicateControlInit.fitCoreEngine(fitCoreLogInBeanDto)){
            return fitCoreLogInBeanDto;
        }else{
            try{
                fitCoreLogInBeanDto.getRequest().getRequestDispatcher(FitCoreStaticFinalLayout.LOGINJSP).forward(fitCoreLogInBeanDto.getRequest(),fitCoreLogInBeanDto.getResponse());
            }catch(ServletException e){
                e.printStackTrace();
            }catch(IOException e){
                e.printStackTrace();
            }
            return null;
        }
    }

    /**
     * Fit log in servlet process service.
     *
     * @param fitCoreLogInBeanDto
     *         the fit core log in bean
     * @param predicateControlInit
     *         the predicate control init
     */
    public static void fitLogInServletProcessService(FitCoreLogInBeanDto fitCoreLogInBeanDto,Engine_Basic_Object_Inter<FitCoreLogInBeanDto,Boolean> predicateControlInit){
        if(predicateControlInit.fitCoreEngine(fitCoreLogInBeanDto)){
            //这里接受的表单值为天来计算的
            int saveTime=Integer.parseInt(fitCoreLogInBeanDto.getFitCoreLogInFormDto().getSavetime());
            int seconds=saveTime*24*60*60;
            Cookie[] cookies=FitCoreCookieURLEncoderProcessImpl.FitCoreCookieURLEncoderProcess(
                    new Cookie[]{
                            new Cookie(
                                    "user",fitCoreLogInBeanDto.getFitCoreLogInFormDto().getUsername()
                                    +"=="+
                                    fitCoreLogInBeanDto.getFitCoreLogInFormDto().getPassword()
                            )
                    },
                    "GBK",
                    FitCoreCookieURLEncoderPredicateImpl::cookieEncoder);
            cookies[0].setMaxAge(seconds);
            fitCoreLogInBeanDto.getResponse().addCookie(cookies[0]);
        }
    }
}
