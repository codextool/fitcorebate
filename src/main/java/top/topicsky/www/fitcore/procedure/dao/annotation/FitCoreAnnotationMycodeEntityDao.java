package top.topicsky.www.fitcore.procedure.dao.annotation;

import org.apache.ibatis.annotations.CacheNamespace;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import top.topicsky.www.fitcore.global.entity.MycodeEntity;

import java.io.Serializable;
import java.util.List;

/**
 * The interface Mycode entity dao.
 */
@Repository
@Transactional
@CacheNamespace(size=512)
public interface FitCoreAnnotationMycodeEntityDao extends Serializable{
    /**
     * Insert int.
     *
     * @param pojo
     *         the pojo
     *
     * @return the int
     */
    int insert(@Param("pojo") MycodeEntity pojo);

    /**
     * Insert selective int.
     *
     * @param pojo
     *         the pojo
     *
     * @return the int
     */
    int insertSelective(@Param("pojo") MycodeEntity pojo);

    /**
     * Insert list int.
     *
     * @param pojo
     *         the pojo
     *
     * @return the int
     */
    int insertList(@Param("pojos") List<MycodeEntity> pojo);

    /**
     * Update int.
     *
     * @param pojo
     *         the pojo
     *
     * @return the int
     */
    int update(@Param("pojo") MycodeEntity pojo);
}
