package top.topicsky.www.fitcore.global.cache.redis.advanced.utilize;

import top.topicsky.www.fitcore.global.cache.redis.advanced.core.FitCoreAdvancedReentrantLockImpl;
import top.topicsky.www.fitcore.global.cache.redis.advanced.core.FitCoreAdvancedSynchronizedImpl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import static top.topicsky.www.fitcore.system.kernel.common.consumer.ConsumerUtil.SOPL;
import static top.topicsky.www.fitcore.system.kernel.common.function.FunctionUtil.OTS;

/**
 * 所在项目名称 ： fitcorebate
 * 操作文件所在包路径　： top.topicsky.www.fitcore.global.cache.redis.advanced.utilize
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 07 月 21 日 17 时 27 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
public class FitCoreAdvancedSynchronizedClientThreadImpl extends Thread{
    int i=0;

    public FitCoreAdvancedSynchronizedClientThreadImpl(int i){
        this.i=i;
    }

    public void run(){
        Date date=new Date();
        DateFormat format=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String time=format.format(date);
        FitCoreAdvancedSynchronizedImpl.setString(OTS.apply(i),time);
        SOPL.accept("[FitCoreAdvancedSynchronizedImpl]:"+FitCoreAdvancedSynchronizedImpl.getString(OTS.apply(i))+" 第："+i+"个线程"+"当前时间："+new Date());
    }
}
