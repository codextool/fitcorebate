package top.topicsky.www.fitcore.global.resource.constant.eternalpublic;

import java.io.Serializable;

/**
 * The type Fit core eternal public extension name.
 */
public class FitCoreEternalPublicExtensionName implements Serializable{
    /**
     * The constant FITPDF.
     */
    public static final String FITPDF=".pdf";
    /**
     * The constant FITTXT.
     */
    public static final String FITTXT=".tst";
    /**
     * The constant FITDOC.
     */
    public static final String FITDOC=".doc";
    /**
     * The constant FITBMP.
     */
    public static final String FITBMP=".bmp";
    /**
     * The constant FITHTML.
     */
    public static final String FITHTML=".html";
    /**
     * The constant FITHTM.
     */
    public static final String FITHTM=".htm";
    /**
     * The constant FITCHM.
     */
    public static final String FITCHM=".chm";
    /**
     * The constant FITJPG.
     */
    public static final String FITJPG=".jpg";
    /**
     * The constant FITJPGE.
     */
    public static final String FITJPGE=".jpeg";
    /**
     * The constant FITPNG.
     */
    public static final String FITPNG=".png";
    /**
     * The constant FITJS.
     */
    public static final String FITJS=".js";
    /**
     * The constant FITJAVA.
     */
    public static final String FITJAVA=".java";
    /**
     * The constant FITC.
     */
    public static final String FITC=".c";
    /**
     * The constant FITRB.
     */
    public static final String FITRB=".rb";
}
