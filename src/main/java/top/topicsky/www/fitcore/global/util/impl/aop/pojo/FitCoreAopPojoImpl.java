package top.topicsky.www.fitcore.global.util.impl.aop.pojo;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import top.topicsky.www.fitcore.global.util.inter.aop.pojo.FitCoreAopPojoInter;

import java.io.Serializable;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.global.util.impl.aop.classic
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 07 日 19 时 30 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Component
@Transactional
public class FitCoreAopPojoImpl implements FitCoreAopPojoInter, Serializable{
    @Override
    public void fitCoreAopPojoBeanInitProxyXa(String args1,Integer args2){
        System.out.println(Thread.currentThread().getStackTrace()[1].getMethodName());
    }

    @Override
    public void fitCoreAopPojoBeanInitProxyXb(String args1,Integer args2){
        System.out.println(Thread.currentThread().getStackTrace()[1].getMethodName());
    }
}
