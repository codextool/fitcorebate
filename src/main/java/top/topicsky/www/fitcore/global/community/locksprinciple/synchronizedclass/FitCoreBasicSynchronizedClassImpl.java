package top.topicsky.www.fitcore.global.community.locksprinciple.synchronizedclass;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;

import static top.topicsky.www.fitcore.system.kernel.common.consumer.ConsumerUtil.SOPL;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.global.community.locksprinciple.synchronizedclass
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 07 月 05 日 13 时 29 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Component
@Transactional
public class FitCoreBasicSynchronizedClassImpl implements FitCoreBasicSynchronizedClassInter, Serializable{
    /**
     * Halt method.
     */
    public static synchronized void haltMethod(){
        SOPL.accept("这里是 haltMethod 方法，是类锁");
        int i=5;
        while(i-->0){
            SOPL.accept(Thread.currentThread().getName()+" : "+i);
            try{
                Thread.sleep(300);
            }catch(InterruptedException ie){
                SOPL.accept(ie.getMessage());
            }
        }
    }

    /* 静态方法和实例方法，类锁和对象锁是两个不一样的锁，控制着不同的区域，它们是互不干扰的
    * 同样，线程获得对象锁的同时，也可以获得该类锁，即同时获得两个锁，这是允许的
    * */
    @Override
    public void initMethod(){
        synchronized(FitCoreBasicSynchronizedClassImpl.class){
            SOPL.accept("这里是 initMethod 方法，是类锁");
            int i=5;
            while(i-->0){
                SOPL.accept(Thread.currentThread().getName()+" : "+i);
                try{
                    Thread.sleep(600);
                }catch(InterruptedException ie){
                    SOPL.accept(ie.getMessage());
                }
            }
        }
    }
}
