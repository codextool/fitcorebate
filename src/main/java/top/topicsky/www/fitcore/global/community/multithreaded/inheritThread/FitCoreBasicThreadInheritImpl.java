package top.topicsky.www.fitcore.global.community.multithreaded.inheritThread;

import static top.topicsky.www.fitcore.system.kernel.common.consumer.ConsumerUtil.SOPL;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.global.community.multithreaded.inheritThread
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 07 月 05 日 09 时 54 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
public class FitCoreBasicThreadInheritImpl extends Thread{
    private Integer integerx=0;

    @Override
    public synchronized void start(){
        super.start();
        SOPL.accept("启动一个新线程，并执行 run() 方法");
    }

    @Override
    public void run(){
        super.run();
        for(int i=0;i<300;i++){
            SOPL.accept(Thread.currentThread().getName()+":"+integerx);
            integerx++;
        }
    }
}

