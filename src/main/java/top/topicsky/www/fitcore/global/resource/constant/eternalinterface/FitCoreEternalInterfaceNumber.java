package top.topicsky.www.fitcore.global.resource.constant.eternalinterface;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.global.resource.constant.eternalinterface
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 22 日 17 时 42 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Component
@Transactional
public interface FitCoreEternalInterfaceNumber extends Serializable{
    /**
     * The constant FITZERO.
     */
    String FITZERO="ZERO";
    /**
     * The constant FITONE.
     */
    String FITONE="ONE";
    /**
     * The constant FITTWO.
     */
    String FITTWO="TWO";
    /**
     * The constant FITTHREE.
     */
    String FITTHREE="THREE";
    /**
     * The constant FITFOUR.
     */
    String FITFOUR="FOUR";
    /**
     * The constant FITFIVE.
     */
    String FITFIVE="FIVE";
    /**
     * The constant FITSIX.
     */
    String FITSIX="SIX";
    /**
     * The constant FITSEVEN.
     */
    String FITSEVEN="SEVEN";
    /**
     * The constant FITEIGHT.
     */
    String FITEIGHT="EIGHT";
    /**
     * The constant FITNINE.
     */
    String FITNINE="NINE";
}
