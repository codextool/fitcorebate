package top.topicsky.www.fitcore.global.exception.impl;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import top.topicsky.www.fitcore.global.exception.inter.FitCoreIllegalMonitorStateExceptionInter;
import top.topicsky.www.fitcore.global.exception.predicate.FitCoreIllegalMonitorStateExceptionPredicateImpl;
import top.topicsky.www.fitcore.global.exception.process.FitCoreIllegalMonitorStateExceptionProcessImpl;

import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.Serializable;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.global.exception.impl
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 19 日 13 时 15 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Component
@Transactional
public abstract class FitCoreIllegalMonitorStateExceptionImpl extends IllegalMonitorStateException implements FitCoreIllegalMonitorStateExceptionInter, Serializable{
    private static final long serialVersionUID=5029348660201239280L;

    /**
     * 非法监视状态异常
     */
    public FitCoreIllegalMonitorStateExceptionImpl(){
        super();
        Runnable runnable=()->System.out.println(FitCoreIllegalMonitorStateExceptionProcessImpl.FitCoreIllegalMonitorStateExceptionInit("Init Message",
                FitCoreIllegalMonitorStateExceptionPredicateImpl::FitCoreIllegalMonitorStateExceptionInit));
        runnable.run();
    }

    /**
     * Instantiates a new Fit core illegal monitor state exception.
     *
     * @param s
     *         the s
     */
    public FitCoreIllegalMonitorStateExceptionImpl(String s){
        super(s);
        Runnable runnable=()->System.out.println(FitCoreIllegalMonitorStateExceptionProcessImpl.FitCoreIllegalMonitorStateExceptionInit("Init Message",
                FitCoreIllegalMonitorStateExceptionPredicateImpl::FitCoreIllegalMonitorStateExceptionInit));
        runnable.run();
    }

    @Override
    public String getMessage(){
        Runnable runnable=()->System.out.println(FitCoreIllegalMonitorStateExceptionProcessImpl.FitCoreIllegalMonitorStateExceptionInit("Init Message",
                FitCoreIllegalMonitorStateExceptionPredicateImpl::FitCoreIllegalMonitorStateExceptionInit));
        runnable.run();
        return super.getMessage();
    }

    @Override
    public String getLocalizedMessage(){
        Runnable runnable=()->System.out.println(FitCoreIllegalMonitorStateExceptionProcessImpl.FitCoreIllegalMonitorStateExceptionInit("Init Message",
                FitCoreIllegalMonitorStateExceptionPredicateImpl::FitCoreIllegalMonitorStateExceptionInit));
        runnable.run();
        return super.getLocalizedMessage();
    }

    @Override
    public synchronized Throwable getCause(){
        Runnable runnable=()->System.out.println(FitCoreIllegalMonitorStateExceptionProcessImpl.FitCoreIllegalMonitorStateExceptionInit("Init Message",
                FitCoreIllegalMonitorStateExceptionPredicateImpl::FitCoreIllegalMonitorStateExceptionInit));
        runnable.run();
        return super.getCause();
    }

    @Override
    public synchronized Throwable initCause(Throwable cause){
        Runnable runnable=()->System.out.println(FitCoreIllegalMonitorStateExceptionProcessImpl.FitCoreIllegalMonitorStateExceptionInit("Init Message",
                FitCoreIllegalMonitorStateExceptionPredicateImpl::FitCoreIllegalMonitorStateExceptionInit));
        runnable.run();
        return super.initCause(cause);
    }

    @Override
    public String toString(){
        Runnable runnable=()->System.out.println(FitCoreIllegalMonitorStateExceptionProcessImpl.FitCoreIllegalMonitorStateExceptionInit("Init Message",
                FitCoreIllegalMonitorStateExceptionPredicateImpl::FitCoreIllegalMonitorStateExceptionInit));
        runnable.run();
        return super.toString();
    }

    @Override
    public void printStackTrace(){
        super.printStackTrace();
        Runnable runnable=()->System.out.println(FitCoreIllegalMonitorStateExceptionProcessImpl.FitCoreIllegalMonitorStateExceptionInit("Init Message",
                FitCoreIllegalMonitorStateExceptionPredicateImpl::FitCoreIllegalMonitorStateExceptionInit));
        runnable.run();
    }

    @Override
    public void printStackTrace(PrintStream s){
        super.printStackTrace(s);
        Runnable runnable=()->System.out.println(FitCoreIllegalMonitorStateExceptionProcessImpl.FitCoreIllegalMonitorStateExceptionInit("Init Message",
                FitCoreIllegalMonitorStateExceptionPredicateImpl::FitCoreIllegalMonitorStateExceptionInit));
        runnable.run();
    }

    @Override
    public void printStackTrace(PrintWriter s){
        super.printStackTrace(s);
        Runnable runnable=()->System.out.println(FitCoreIllegalMonitorStateExceptionProcessImpl.FitCoreIllegalMonitorStateExceptionInit("Init Message",
                FitCoreIllegalMonitorStateExceptionPredicateImpl::FitCoreIllegalMonitorStateExceptionInit));
        runnable.run();
    }

    @Override
    public synchronized Throwable fillInStackTrace(){
        Runnable runnable=()->System.out.println(FitCoreIllegalMonitorStateExceptionProcessImpl.FitCoreIllegalMonitorStateExceptionInit("Init Message",
                FitCoreIllegalMonitorStateExceptionPredicateImpl::FitCoreIllegalMonitorStateExceptionInit));
        runnable.run();
        return super.fillInStackTrace();
    }

    @Override
    public StackTraceElement[] getStackTrace(){
        Runnable runnable=()->System.out.println(FitCoreIllegalMonitorStateExceptionProcessImpl.FitCoreIllegalMonitorStateExceptionInit("Init Message",
                FitCoreIllegalMonitorStateExceptionPredicateImpl::FitCoreIllegalMonitorStateExceptionInit));
        runnable.run();
        return super.getStackTrace();
    }

    @Override
    public void setStackTrace(StackTraceElement[] stackTrace){
        super.setStackTrace(stackTrace);
        Runnable runnable=()->System.out.println(FitCoreIllegalMonitorStateExceptionProcessImpl.FitCoreIllegalMonitorStateExceptionInit("Init Message",
                FitCoreIllegalMonitorStateExceptionPredicateImpl::FitCoreIllegalMonitorStateExceptionInit));
        runnable.run();
    }
}
