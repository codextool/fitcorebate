package top.topicsky.www.fitcore.global.exception.impl;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;
import top.topicsky.www.fitcore.global.exception.inter.FitCoreGlobalExceptionHandlerInter;
import top.topicsky.www.fitcore.global.exception.predicate.FitCoreGlobalExceptionHandlerPredicateImpl;
import top.topicsky.www.fitcore.global.exception.process.FitCoreGlobalExceptionHandlerProcessImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.Serializable;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.global.exception.impl
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 17 日 11 时 37 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Component
@Transactional
public abstract class FitCoreGlobalExceptionHandlerImpl implements FitCoreGlobalExceptionHandlerInter, HandlerExceptionResolver, Serializable{
    /**
     * Instantiates a new Fit core global exception handler.
     */
    public FitCoreGlobalExceptionHandlerImpl(){
    }

    /**
     * 这里是全局异常处理类
     * 第4个参数表示对哪种类型的异常进行处理，如果想同时对多种异常进行处理，可以把它换成一个异常数组
     */
    @Override
    public ModelAndView resolveException(HttpServletRequest httpServletRequest,HttpServletResponse httpServletResponse,Object o,Exception e){
        Runnable runnable=()->System.out.println(FitCoreGlobalExceptionHandlerProcessImpl.FitCoreGlobalExceptionHandlerInit("Init Message",
                FitCoreGlobalExceptionHandlerPredicateImpl::FitCoreGlobalExceptionHandlerInit));
        runnable.run();
        System.out.println(e.toString());
        return new ModelAndView("error/page_500");
    }
}
