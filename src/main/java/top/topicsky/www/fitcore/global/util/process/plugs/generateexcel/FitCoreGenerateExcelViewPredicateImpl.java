package top.topicsky.www.fitcore.global.util.process.plugs.generateexcel;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.support.WebApplicationContextUtils;
import top.topicsky.www.fitcore.global.dto.FitCoreLogInBeanDto;
import top.topicsky.www.fitcore.global.dto.FitCoreLogInFormDto;
import top.topicsky.www.fitcore.procedure.service.inter.FitCoreUserServiceInter;
import top.topicsky.www.fitcore.procedure.service.inter.FitCoreUtilServiceInter;
import top.topicsky.www.fitcore.system.kernel.engine.Engine_Basic_Object_Inter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.Serializable;
import java.util.Map;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.global.util.process.plugs.generateexcel
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 17 日 13 时 21 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Component
@Transactional
public class FitCoreGenerateExcelViewPredicateImpl implements Serializable{
    /**
     * Fit core generate excel view predicate impl init map.
     *
     * @param httpServletRequest
     *         the http servlet request
     * @param httpServletResponse
     *         the http servlet response
     * @param predicateControlInit
     *         the predicate control init
     *
     * @return the map
     */
    public static Map<String,String> fitCoreGenerateExcelViewPredicateImplInit(HttpServletRequest httpServletRequest,HttpServletResponse httpServletResponse,Engine_Basic_Object_Inter<FitCoreLogInBeanDto,Map<String,String>> predicateControlInit){
        BeanFactory beans=WebApplicationContextUtils.getWebApplicationContext(httpServletRequest.getSession().getServletContext());
        FitCoreUtilServiceInter fitCoreUtilServiceImplInit=(FitCoreUtilServiceInter)beans.getBean("fitCoreUtilServiceImpl");
        FitCoreUserServiceInter fitCoreUserServiceImplInit=(FitCoreUserServiceInter)beans.getBean("fitCoreUserServiceImpl");
        FitCoreLogInBeanDto fitCoreLogInBeanDto=new FitCoreLogInBeanDto();
        FitCoreLogInFormDto fitCoreLogInFormDto=new FitCoreLogInFormDto();
        fitCoreLogInBeanDto.setFitCoreUtilServiceInterInit(fitCoreUtilServiceImplInit);
        fitCoreLogInBeanDto.setFitCoreUserServiceImplInit(fitCoreUserServiceImplInit);
        //fitCoreLogInFormDto.setUsername(httpServletRequest.getParameter("username"));
        fitCoreLogInFormDto.setUsername("潘小平");
        fitCoreLogInBeanDto.setFitCoreLogInFormDto(fitCoreLogInFormDto);
        return predicateControlInit.fitCoreEngine(fitCoreLogInBeanDto);
    }
}
