package top.topicsky.www.fitcore.global.community.multithreaded.threadpoolintermediateimplement;

import org.apache.activemq.thread.Task;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.global.community.multithreaded.threadpoolimplement
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 07 月 05 日 17 时 14 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Component
@Transactional
public class FitCoreInterMediateThreadTaskBeanImpl extends FitCoreInterMediateThreadTaskImpl implements Serializable{
    @Override
    public Task[] taskCore() throws Exception{
        return new Task[0];
    }

    @Override
    protected boolean useDb(){
        return false;
    }

    @Override
    protected boolean needExecuteImmediate(){
        return false;
    }

    @Override
    public String info(){
        return "这是任务简要信息";
    }
}
