package top.topicsky.www.fitcore.global.community.locksprinciple.synchronizedmethod;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.global.community.locksprinciple.synchronizedmethod
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 07 月 05 日 13 时 43 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Component
@Transactional
public interface FitCoreBasicSynchronizedMainInter extends Serializable{
    /**
     * Init topicsky.
     */
/*这暂时只是一个普通的接口*/
    void initTopicsky();
}
