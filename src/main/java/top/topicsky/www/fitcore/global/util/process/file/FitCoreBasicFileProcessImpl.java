package top.topicsky.www.fitcore.global.util.process.file;

import top.topicsky.www.fitcore.system.kernel.engine.consumer.FitCoreBasicConsumerInter;

import java.io.File;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.global.util.process.file
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 07 月 04 日 15 时 36 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
public class FitCoreBasicFileProcessImpl{
    /**
     * List process.
     *
     * @param path
     *         the path
     * @param listPredicate
     *         the list predicate
     */
    public static void listProcess(File path,FitCoreBasicConsumerInter<File> listPredicate){
        listPredicate.accept(path);
    }

    /**
     * Copy process.
     *
     * @param source
     *         the source
     * @param target
     *         the target
     * @param copyPredicate
     *         the copy predicate
     */
    public static void copyProcess(File source,File target,FitCoreBasicConsumerInter<File[]> copyPredicate){
        File[] files={source,target};
        copyPredicate.accept(files);
    }
}
