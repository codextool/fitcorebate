package top.topicsky.www.fitcore.global.util.process.customformtags.refixform;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import top.topicsky.www.fitcore.global.resource.constant.FitCoreStaticFinalVariable;
import top.topicsky.www.fitcore.system.kernel.engine.Engine_Basic_Object_Inter;

import java.io.Serializable;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.global.util.process.customformtags.refixform
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 21 日 17 时 08 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Component
@Transactional
public class FitCoreFormProcessImpl implements Serializable{
    /**
     * Fit core form process pack.
     *
     * @param <OBJ>
     *         the type parameter
     * @param model
     *         the model
     * @param obj
     *         the obj
     * @param objName
     *         the obj name
     * @param formPredicate
     *         the form predicate
     */
    public static <OBJ> void FitCoreFormProcessPack(Model model,
                                                    OBJ obj,
                                                    String objName,
                                                    Engine_Basic_Object_Inter<OBJ,String> formPredicate){
        if(FitCoreStaticFinalVariable.SUCCESS.equals(formPredicate.fitCoreEngine(obj))){
            model.addAttribute(objName,obj);
            System.out.println(FitCoreStaticFinalVariable.SUCCESS);
        }
    }
}
