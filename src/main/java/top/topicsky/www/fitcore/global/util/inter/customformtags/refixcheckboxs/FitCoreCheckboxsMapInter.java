package top.topicsky.www.fitcore.global.util.inter.customformtags.refixcheckboxs;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.global.util.inter.customformtags.refixcheckboxs
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 21 日 17 时 08 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Component
@Transactional
public interface FitCoreCheckboxsMapInter extends Serializable{
    /**
     * Fit core checkboxs pack string.
     *
     * @param <OBJ>
     *         the type parameter
     * @param <TX>
     *         the type parameter
     * @param <TY>
     *         the type parameter
     * @param <TZ>
     *         the type parameter
     * @param <TZV>
     *         the type parameter
     * @param <TQ>
     *         the type parameter
     * @param <TP>
     *         the type parameter
     * @param <TS>
     *         the type parameter
     * @param <TF>
     *         the type parameter
     * @param <TR>
     *         the type parameter
     * @param model
     *         the model
     * @param listchoice
     *         the listchoice
     * @param mapContent
     *         the list content
     * @param obj
     *         the obj
     * @param objProperty
     *         the obj property
     * @param choiceName
     *         the choice name
     * @param contentName
     *         the content name
     * @param pageForword
     *         the page forword
     *
     * @return the string
     */
    <OBJ,TX,TY,TZ,TZV,TQ,TP,TS,TF,TR> TR FitCoreCheckboxsMapPack(
            /* spring 模型*/
            TX model,
                                      /*多选框需要被选中的值名称*/
            List<TY> listchoice,
                                      /*多选框需要被展示的所有值名称*/
            Map<TZ,TZV> mapContent,
                                      /*多选框的实际值存储对象*/
            OBJ obj,
                                      /*多选框的实际值存储对象的需要存之的属性*/
            TQ objProperty,
                                      /*存在model中的对象名称，被model form 识别*/
            TP choiceName,
                                      /*存所有显示值的名称 EL取值 得到*/
            TS contentName,
                                       /*存页面转发值*/
            TF pageForword);
}
