package top.topicsky.www.fitcore.global.util.impl.customformtags.refixcheckboxs;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import top.topicsky.www.fitcore.global.util.inter.customformtags.refixcheckboxs.FitCoreCheckboxsMapInter;
import top.topicsky.www.fitcore.global.util.predicate.customformtags.refixcheckboxs.FitCoreCheckboxsMapPredicateImpl;
import top.topicsky.www.fitcore.global.util.process.customformtags.refixcheckboxs.FitCoreCheckboxsMapProcessImpl;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.global.util.impl.customformtags.refixcheckboxs
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 21 日 17 时 08 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Component
@Transactional
public class FitCoreCheckboxsMapImpl implements FitCoreCheckboxsMapInter, Serializable{
    public <OBJ,TX,TY,TZ,TZV,TQ,TP,TS,TF,TR> TR FitCoreCheckboxsMapPack(TX model,
                                                                        List<TY> listchoice,
                                                                        Map<TZ,TZV> mapContent,
                                                                        OBJ obj,
                                                                        TQ objProperty,
                                                                        TP choiceName,
                                                                        TS contentName,
                                                                        TF pageForword){
        FitCoreCheckboxsMapProcessImpl.FitCoreCheckboxsMapProcessPack(model,
                listchoice,
                obj,
                objProperty,
                choiceName,
                FitCoreCheckboxsMapPredicateImpl::FitCoreCheckboxsMapPredicatePack);
        FitCoreCheckboxsMapProcessImpl.FitCoreCheckboxsMapProcessContentPack(model,
                mapContent,
                contentName,
                FitCoreCheckboxsMapPredicateImpl::FitCoreCheckboxsMapPredicateContentPack);
        return (TR)pageForword;
    }
}
