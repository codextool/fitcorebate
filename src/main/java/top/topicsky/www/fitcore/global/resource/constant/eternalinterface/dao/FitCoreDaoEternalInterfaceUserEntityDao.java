package top.topicsky.www.fitcore.global.resource.constant.eternalinterface.dao;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.global.resource.constant.eternalinterface.dao
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 07 月 06 日 18 时 35 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Component
@Transactional
public interface FitCoreDaoEternalInterfaceUserEntityDao extends Serializable{
    /**
     * The constant INSERTCONDITION.
     */
    String INSERTCONDITION="INSERT INTO fitcoreuserentity ("+
            "fitCoreId,"+
            "fitCoreUuid,"+
            "fitCoreImage,"+
            "fitCoreTheme,"+
            "fitCoreCname,}"+
            "fitCoreEname,"+
            "fitCoreDisplayname,"+
            "fitCoreAge,"+
            "fitCoreBirthday,"+
            "fitCoreEmail,"+
            "fitCorePassword,"+
            "fitCorePhone,"+
            "fitCoreTelphone,"+
            "fitCoreCountry,"+
            "fitCoreProvince,"+
            "fitCoreCity,"+
            "fitCoreStreet,"+
            "fitCoreCompany,"+
            "fitCoreDepartment,"+
            "fitCoreIp"+
            ") "+
            "VVALUES"+
            "(#{fitCoreId},"+
            "#{fitCoreUuid},"+
            "#{fitCoreImage},"+
            "#{fitCoreTheme},"+
            "#{fitCoreCname},"+
            "#{fitCoreEname},"+
            "#{fitCoreDisplayname},"+
            "#{fitCoreAge},"+
            "#{fitCoreBirthday},"+
            "#{fitCoreEmail},"+
            "#{fitCorePassword},"+
            "#{fitCorePhone},"+
            "#{fitCoreTelphone},"+
            "#{fitCoreCountry},"+
            "#{fitCoreProvince},"+
            "#{fitCoreCity},"+
            "#{fitCoreStreet},"+
            "#{fitCoreCompany},"+
            "#{fitCoreDepartment},"+
            "#{fitCoreIp})";
    /**
     * The constant DELETECONDITION.
     */
    String DELETECONDITION="DELETE FROM fitcoreuserentity WHERE fitCoreId=#{fitCoreId}";
    /**
     * The constant UPDATECONDITION.
     */
    String UPDATECONDITION="UPDATE fitcoreuserentity SET "+
            "fitCoreUuid=#{fitCoreUuid},"+
            "fitCoreImage=#{fitCoreImage},"+
            "fitCoreTheme=#{fitCoreTheme},"+
            "fitCoreCname=#{fitCoreCname},"+
            "fitCoreEname=#{fitCoreEname},"+
            "fitCoreDisplayname=#{fitCoreDisplayname},"+
            "fitCoreAge=#{fitCoreAge},"+
            "fitCoreBirthday=#{fitCoreBirthday},"+
            "fitCoreEmail=#{fitCoreEmail},"+
            "fitCorePassword=#{fitCorePassword},"+
            "fitCorePhone=#{fitCorePhone},"+
            "fitCoreTelphone=#{fitCoreTelphone},"+
            "fitCoreCountry=#{fitCoreCountry},"+
            "fitCoreProvince=#{fitCoreProvince},"+
            "fitCoreCity=#{fitCoreCity},"+
            "fitCoreStreet=#{fitCoreStreet},"+
            "fitCoreCompany=#{fitCoreCompany},"+
            "fitCoreDepartment=#{fitCoreDepartment},"+
            "fitCoreIp=#{fitCoreIp} "+
            "WHERE fitCoreId=#{fitCoreId}";
    /**
     * The constant SELECTCONDITION.
     */
    String SELECTCONDITION="SELEct * FROM fitcoreuserentity WHERE fitCoreId=#{fitCoreId}";
    /**
     * The constant SELECTALLCONDITION.
     */
    String SELECTALLCONDITION="SELEct * FROM fitcoreuserentity";
}
