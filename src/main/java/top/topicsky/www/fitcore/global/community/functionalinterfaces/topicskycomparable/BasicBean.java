package top.topicsky.www.fitcore.global.community.functionalinterfaces.topicskycomparable;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.xml.bind.annotation.XmlRootElement;
/**
 * 所在项目名称 ： topicskycode
 * 操作文件所在包路径　： languagefeature.functionalinterfaces.topicskycomparable
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 07 月 03 日 17 时 52 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */

/**
 * 使用 @Data 相当于同时使用了@ToString、@EqualsAndHashCode、@Getter、@Setter和@RequiredArgsConstructor这些注解
 *
 * @Cleanup 他可以帮我们在需要释放的资源位置自动加上释放代码
 * @Synchronized 可以帮我们在方法上添加同步代码块
 * @XmlRootElement 定义了注解注入实例
 */
@Getter(AccessLevel.PUBLIC)
@Setter(AccessLevel.PUBLIC)
@ToString(callSuper=true)
/**
 * 这里如果不写false,会报错,
 * Error:(21, 1) 警告: Generating equals/hashCode implementation but without a call to superclass,
 * even though this class does not extend java.lang.Object. If this is intentional,
 * add '@EqualsAndHashCode(callSuper=false)' to your type.
 */
@EqualsAndHashCode(callSuper=false)
@RequiredArgsConstructor
@Slf4j
@Repository
@Transactional
@XmlRootElement
public class BasicBean implements Comparable<BasicBean>{
    /*
    * 让需要进行排序的对象实现Comparable接口
    * 重写其中的compareTo(T o)方法
    * 在其中定义排序规则
    * 那么就可以直接调用 java.util.Arrays.sort() 来排序对象数组
    * */
    private String topicskyName;
    private Integer topicskyId;
    private Double topicskyReport;

    /**
     * Instantiates a new Basic bean.
     *
     * @param topicskyUuid
     *         the topicsky uuid
     * @param topicskyId
     *         the topicsky id
     * @param topicskyReport
     *         the topicsky report
     */
    public BasicBean(String topicskyUuid,Integer topicskyId,Double topicskyReport){
        this.topicskyName=topicskyUuid;
        this.topicskyId=topicskyId;
        this.topicskyReport=topicskyReport;
    }

    @Override
    public int compareTo(BasicBean basicBean){
        //topicskyReport 是 private 的，为什么能够直接调用,这是因为在Student类内部
        if(this.topicskyReport>basicBean.topicskyReport){
            //由高到底排序
            return -1;
        }else if(this.topicskyReport<basicBean.topicskyReport){
            return 1;
        }else{
            if(this.topicskyId>basicBean.topicskyId){
                //由底到高排序
                return 1;
            }else if(this.topicskyId<basicBean.topicskyId){
                return -1;
            }else{
                return 0;
            }
        }
    }
}
