package top.topicsky.www.fitcore.global.resource.constant.eternalpublic;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;

/**
 * The type Fit core eternal public variable.
 */
@Component
@Transactional
public class FitCoreEternalPublicVariable implements Serializable{
    /**
     * The constant FITTRUE.
     */
    public static final Boolean FITTRUE=true;
    /**
     * The constant FITFALSE.
     */
    public static final Boolean FITFALSE=false;
    /**
     * The constant SUCCESS.
     */
    public static final String SUCCESS="SUCCESS(成功)";
    /**
     * The constant FAILURE.
     */
    public static final String FAILURE="FAILURE(失败)";
    /**
     * The constant INITVOIDSTATIC.
     */
    public static final String INITVOIDSTATIC="INITVOIDSTATIC(函数静态)";
    /**
     * The constant INITBASICSTATIC.
     */
    public static final String INITBASICSTATIC="INITBASICSTATIC(基础静态)";
    /**
     * The constant INITNOPARAMSTATIC.
     */
    public static final String INITNOPARAMSTATIC="INITNOPARAMSTATIC(无参数带返回静态)";
    /**
     * The constant INITNORETURNSTATIC.
     */
    public static final String INITNORETURNSTATIC="INITNORETURNSTATIC(无返回带参数静态)";
    /**
     * The constant INITVOIDDAFAULT.
     */
    public static final String INITVOIDDAFAULT="INITVOIDDAFAULT(函数默认)";
    /**
     * The constant INITBASICDAFAULT.
     */
    public static final String INITBASICDAFAULT="INITBASICDAFAULT(基础默认)";
    /**
     * The constant INITNOPARAMDAFAULT.
     */
    public static final String INITNOPARAMDAFAULT="INITNOPARAMDAFAULT(无参数带返回默认)";
    /**
     * The constant INITNORETURNDAFAULT.
     */
    public static final String INITNORETURNDAFAULT="INITNORETURNDAFAULT(无返回带参数默认)";
}
