package top.topicsky.www.fitcore.global.util.process.customformtags.refixradiobutton;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import top.topicsky.www.fitcore.global.resource.constant.FitCoreStaticFinalVariable;
import top.topicsky.www.fitcore.system.kernel.engine.Engine_Basic_Object_Inter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.global.util.process.customformtags.refixradiobutton
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 21 日 17 时 08 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Component
@Transactional
public class FitCoreRadiobuttonProcessImpl implements Serializable{
    /**
     * Fit core radiobutton process pack.
     *
     * @param <OBJ>
     *         the type parameter
     * @param <TX>
     *         the type parameter
     * @param <TY>
     *         the type parameter
     * @param <TQ>
     *         the type parameter
     * @param <TP>
     *         the type parameter
     * @param <TS>
     *         the type parameter
     * @param <TF>
     *         the type parameter
     * @param <TR>
     *         the type parameter
     * @param model
     *         the model
     * @param obj
     *         the obj
     * @param objPropert
     *         the obj propert
     * @param objValue
     *         the obj value
     * @param choiceName
     *         the choice name
     * @param buttonPredicate
     *         the button predicate
     */
    public static <OBJ,TX,TY,TQ,TP,TS,TF,TR> void FitCoreRadiobuttonProcessPack(TX model,
                                                                                OBJ obj,
                                                                                TQ objPropert,
                                                                                TP objValue,
                                                                                TS choiceName,
                                                                                Engine_Basic_Object_Inter<List<TY>,TR> buttonPredicate){
        List<TY> beanObject=new ArrayList<>();
        beanObject.add((TY)obj);
        beanObject.add((TY)objPropert);
        beanObject.add((TY)objValue);
        /*被选择的可以是空，不需要判断*/
        if(FitCoreStaticFinalVariable.SUCCESS.equals(buttonPredicate.fitCoreEngine(beanObject))){
            ((Model)model).addAttribute((String)choiceName,obj);
            System.out.println(FitCoreStaticFinalVariable.SUCCESS);
        }
    }
}
