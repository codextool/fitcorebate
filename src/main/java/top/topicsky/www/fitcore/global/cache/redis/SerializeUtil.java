package top.topicsky.www.fitcore.global.cache.redis;

import java.io.*;

/**
 * 所在项目名称 ： fitcorebate
 * 操作文件所在包路径　： top.topicsky.www.fitcore.global.cache.redis
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 07 月 20 日 17 时 44 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
public class SerializeUtil{
    public static byte[] serialize(Object value){
        if(value==null){
            throw new NullPointerException("Can't serialize null");
        }
        byte[] rv=null;
        ByteArrayOutputStream bos=null;
        ObjectOutputStream os=null;
        try{
            bos=new ByteArrayOutputStream();
            os=new ObjectOutputStream(bos);
            os.writeObject(value);
            os.close();
            bos.close();
            rv=bos.toByteArray();
        }catch(Exception e){
            e.printStackTrace();
        }finally{
            close(os);
            close(bos);
        }
        return rv;
    }

    public static Object deserialize(byte[] in){
        return deserialize(in,Object.class);
    }

    @SuppressWarnings("unchecked")
    public static <T> T deserialize(byte[] in,Class<T> requiredType){
        Object rv=null;
        ByteArrayInputStream bis=null;
        ObjectInputStream is=null;
        try{
            if(in!=null){
                bis=new ByteArrayInputStream(in);
                is=new ObjectInputStream(bis);
                rv=is.readObject();
            }
        }catch(Exception e){
            e.printStackTrace();
        }finally{
            close(is);
            close(bis);
        }
        return (T)rv;
    }

    private static void close(Closeable closeable){
        if(closeable!=null)
            try{
                closeable.close();
            }catch(IOException e){
                e.printStackTrace();
            }
    }
}
