package top.topicsky.www.fitcore.global.resource.constant.eternalenum;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.global.resource.constant.eternalinterface
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 22 日 17 时 42 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
public enum FitCoreEternalEnumVariable{
    /**
     * Success fit core eternal enum variable.
     */
    SUCCESS,
    /**
     * Failure fit core eternal enum variable.
     */
    FAILURE,
    /**
     * Initvoidstatic fit core eternal enum variable.
     */
    INITVOIDSTATIC,
    /**
     * Initbasicstatic fit core eternal enum variable.
     */
    INITBASICSTATIC,
    /**
     * Initnoparamstatic fit core eternal enum variable.
     */
    INITNOPARAMSTATIC,
    /**
     * Initnoreturnstatic fit core eternal enum variable.
     */
    INITNORETURNSTATIC,
    /**
     * Initvoiddafault fit core eternal enum variable.
     */
    INITVOIDDAFAULT,
    /**
     * Initbasicdafault fit core eternal enum variable.
     */
    INITBASICDAFAULT,
    /**
     * Initnoparamdafault fit core eternal enum variable.
     */
    INITNOPARAMDAFAULT,
    /**
     * Initnoreturndafault fit core eternal enum variable.
     */
    INITNORETURNDAFAULT;
}
