-- auto Generated on 2017-06-13 11:40:34
-- DROP TABLE IF EXISTS `fitcoreuserentity`;
CREATE TABLE `fitcoreuserentity`(
    `fitcoreid` BIGINT (15) UNIQUE NOT NULL AUTO_INCREMENT COMMENT 'fitCoreId',
    `fitcoreuuid` VARCHAR (255) UNIQUE NOT NULL DEFAULT '' COMMENT 'fitCoreUuid',
    `fitcoreimage` VARCHAR (255) NOT NULL DEFAULT '' COMMENT 'fitCoreImage',
    `fitcoretheme` VARCHAR (255) NOT NULL DEFAULT '' COMMENT 'fitCoreTheme',
    `fitcorecname` VARCHAR (255) UNIQUE NOT NULL DEFAULT '' COMMENT 'fitCoreCname',
    `fitcoreename` VARCHAR (255) NOT NULL DEFAULT '' COMMENT 'fitCoreEname',
    `fitcoredisplayname` VARCHAR (255) NOT NULL DEFAULT '' COMMENT 'fitCoreDisplayname',
    `fitcoreage` VARCHAR (50) NOT NULL DEFAULT '' COMMENT 'fitCoreAge',
    `fitcorebirthday` VARCHAR (255) NOT NULL DEFAULT '' COMMENT 'fitCoreBirthday',
    `fitcoreemail` VARCHAR (255) UNIQUE NOT NULL DEFAULT '' COMMENT 'fitCoreEmail',
    `fitcorepassword` VARCHAR (255) NOT NULL DEFAULT '' COMMENT 'fitCorePassword',
    `fitcorephone` VARCHAR (32) UNIQUE NOT NULL DEFAULT '' COMMENT 'fitCorePhone',
    `fitcoretelphone` VARCHAR (32) UNIQUE NOT NULL DEFAULT '' COMMENT 'fitCoreTelphone',
    `fitcorecountry` VARCHAR (255) NOT NULL DEFAULT '' COMMENT 'fitCoreCountry',
    `fitcoreprovince` VARCHAR (255) NOT NULL DEFAULT '' COMMENT 'fitCoreProvince',
    `fitcorecity` VARCHAR (255) NOT NULL DEFAULT '' COMMENT 'fitCoreCity',
    `fitcorestreet` VARCHAR (512) NOT NULL DEFAULT '' COMMENT 'fitCoreStreet',
    `fitcorecompany` VARCHAR (255) NOT NULL DEFAULT '' COMMENT 'fitCoreCompany',
    `fitcoredepartment` VARCHAR (512) NOT NULL DEFAULT '' COMMENT 'fitCoreDepartment',
    `fitcoreip` VARCHAR (128) UNIQUE NOT NULL DEFAULT '' COMMENT 'fitCoreIp',
    PRIMARY KEY (`fitcoreid`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT '`fitcoreuserentity`';
