package top.topicsky.www.fitcore.global.community.multithreaded.implementexecutor;


import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import static top.topicsky.www.fitcore.system.kernel.common.consumer.ConsumerUtil.SOPL;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.global.community.multithreaded.implementexecutor
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 07 月 05 日 09 时 58 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Component
@Transactional
public class FitCoreBasicExecutorMainImpl implements FitCoreBasicExecutorMainInter, Serializable{
    /**
     * Invock init topicsky.
     *
     * @throws Exception
     *         the exception
     */

    public void invockInitTopicsky() throws Exception{
        /*测试框架*/
        this.initTopicsky();
    }

    @Override
    public void initTopicsky() throws Exception{
        SOPL.accept("----程序开始运行----");
        Date datestart=new Date();
        /*线程池里面线程上限*/
        int taskSize=5;
        /*创建一个线程池*/
        ExecutorService pool=Executors.newFixedThreadPool(taskSize);
        /*创建多个有返回值的任务*/
        List<Future> list=new ArrayList<Future>();
        for(int i=0;i<taskSize;i++){
            Callable c=new FitCoreBasicExecutorImpl("在第"+i+"条线程里面进行任务");
            /*执行任务并获取Future对象*/
            Future f=pool.submit(c);
            list.add(f);
        }
        /*关闭线程池*/
        pool.shutdown();

        /*获取所有并发任务的运行结果*/
        for(Future f : list){
            /*从Future对象上获取任务的返回值，并输出到控制台*/
            SOPL.accept(">>>"+f.get().toString());
        }
        Date dateend=new Date();
        SOPL.accept("----程序结束运行----，程序运行时间【"+(dateend.getTime()-datestart.getTime())+"毫秒】");
    }
}
