package top.topicsky.www.fitcore.global.util.impl.aop.classic.invock;

import org.aopalliance.aop.Advice;
import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.springframework.aop.Advisor;
import org.springframework.aop.AfterReturningAdvice;
import org.springframework.aop.MethodBeforeAdvice;
import org.springframework.aop.framework.ProxyFactory;
import org.springframework.aop.support.DefaultPointcutAdvisor;
import org.springframework.aop.support.JdkRegexpMethodPointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import top.topicsky.www.fitcore.global.util.impl.aop.classic.FitCoreAopClassicImpl;
import top.topicsky.www.fitcore.global.util.inter.aop.classic.FitCoreAopClassicInter;
import top.topicsky.www.fitcore.global.util.inter.aop.classic.invock.FitCoreAopClassicInvockInter;

import java.io.Serializable;
import java.lang.reflect.Method;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.global.util.impl.aop.classic.invock
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 08 日 14 时 23 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Component
@Transactional
@RequestMapping("/fitcoreaop")
public class FitCoreAopClassicInvockImpl implements FitCoreAopClassicInvockInter, Serializable{
    /*#####################################注入控制层######################################################*/
    /*#####################################注入控制层######################################################*/
    /*#####################################注入业务层######################################################*/
    @Qualifier("fitCoreAopClassicImplProxyXa")
    @Autowired
    private FitCoreAopClassicInter fitCoreAopClassicImplProxyXa;
    @Qualifier("fitCoreAopClassicImplProxyXb")
    @Autowired
    private FitCoreAopClassicInter fitCoreAopClassicImplProxyXb;
    @Qualifier("fitCoreAopClassicImplProxyXc")
    @Autowired
    private FitCoreAopClassicInter fitCoreAopClassicImplProxyXc;
    @Qualifier("fitCoreAopClassicImplProxyXd")
    @Autowired
    private FitCoreAopClassicInter fitCoreAopClassicImplProxyXd;
    @Qualifier("fitCoreAopClassicImplProxyXe")
    @Autowired
    private FitCoreAopClassicInter fitCoreAopClassicImplProxyXe;
    @Qualifier("fitCoreAopClassicImplProxyXf")
    @Autowired
    private FitCoreAopClassicInter fitCoreAopClassicImplProxyXf;
    @Qualifier("fitCoreAopClassicImplProxyXg")
    @Autowired
    private FitCoreAopClassicInter fitCoreAopClassicImplProxyXg;
    /*#####################################注入业务层######################################################*/
    /*#####################################注入持久层######################################################*/

    /*#####################################注入持久层######################################################*/
    /*#####################################注入数据层######################################################*/

    /*#####################################注入数据层######################################################*/
    /*#####################################依赖配置类######################################################*/

    /*#####################################依赖配置类######################################################*/
    /*#####################################自定义注解######################################################*/

    /*#####################################自定义注解######################################################*/

    @Override
    @RequestMapping(value="/classicha", method=RequestMethod.GET)
    public String fitCoreAopClassicImplInvock_ProxyXa(){
        fitCoreAopClassicImplProxyXa.fitCoreAopClassicBeanInitProxyXa();
        return "MyCode";
    }

    @Override
    @RequestMapping(value="/classichb", method=RequestMethod.GET)
    public String fitCoreAopClassicImplInvock_ProxyXb(){
        fitCoreAopClassicImplProxyXb.fitCoreAopClassicBeanInitProxyXb();
        return "MyCode";
    }

    @Override
    @RequestMapping(value="/classichc", method=RequestMethod.GET)
    public String fitCoreAopClassicImplInvock_ProxyXc(){
        fitCoreAopClassicImplProxyXc.fitCoreAopClassicBeanInitProxyXc();
        return "MyCode";
    }

    @Override
    @RequestMapping(value="/classichd", method=RequestMethod.GET)
    public String fitCoreAopClassicImplInvock_ProxyXd(){
        fitCoreAopClassicImplProxyXd.fitCoreAopClassicBeanInitProxyXd();
        return "MyCode";
    }

    @Override
    @RequestMapping(value="/classiche", method=RequestMethod.GET)
    public String fitCoreAopClassicImplInvock_ProxyXe(){
        fitCoreAopClassicImplProxyXe.fitCoreAopClassicBeanInitProxyXe();
        return "MyCode";
    }

    @Override
    @RequestMapping(value="/classichf", method=RequestMethod.GET)
    public String fitCoreAopClassicImplInvock_ProxyXf(){
        //如果你对动态代理有过了解了，对下面的代码会很好理解的
        //ProxyFactoryBean的功能比ProxyFactory强
        ProxyFactory factory=new ProxyFactory();
        //给代理工厂一个原型对象
        factory.setTarget(fitCoreAopClassicImplProxyXf);
        //构造切面
        //切面 = 切点 + 通知
        //切点
        JdkRegexpMethodPointcut cut=new JdkRegexpMethodPointcut();
        //可以直接给方法全名
        //cut.setPattern("cn.hncu.javaImpl.Person.run");
        //或者给正则表达式
        //.号匹配除"\r\n"之外的任何单个字符。*号代表零次或多次匹配前面的字符或子表达式
        cut.setPattern(".*fitCoreAopClassicBeanInit.*");
        //通知
        Advice advice=new MethodInterceptor(){
            //动态代理中的那个方法
            @Override
            public Object invoke(MethodInvocation methodInv) throws Throwable{
                System.out.println("这里是纯底层拦截——前面...");
                Object resObj=methodInv.proceed();
                System.out.println("这里是纯底层拦截——后面...");
                return resObj;
            }
        };
        //切面 = 切点 + 通知
        Advisor advisor=new DefaultPointcutAdvisor(cut,advice);
        factory.addAdvisor(advisor);//给代理工厂一个切面
        FitCoreAopClassicImpl fitCoreAopClassicImplProxyXf=(FitCoreAopClassicImpl)factory.getProxy();//从代理工厂中获取一个代理后的对象
        fitCoreAopClassicImplProxyXf.fitCoreAopClassicBeanInitProxyXf();
        return "MyCode";
    }

    @Override
    @RequestMapping(value="/classichg", method=RequestMethod.GET)
    public String fitCoreAopClassicImplInvock_ProxyXg(){
        ProxyFactory factory=new ProxyFactory();
        //给代理工厂一个原型对象
        factory.setTarget(fitCoreAopClassicImplProxyXg);
        //切面 = 切点 + 通知
        //切点
        JdkRegexpMethodPointcut cut=new JdkRegexpMethodPointcut();
        //可以配置多个正则表达式
        cut.setPatterns(new String[]{".*fitCoreAopClassicBeanInit.*"});
        //通知 前切面---不需要放行，原方法也能执行
        Advice before=new MethodBeforeAdvice(){
            @Override
            public void before(Method method,Object[] args,Object obj)
                    throws Throwable{
                System.out.println("这里是纯底层拦截另一种实现方式——前面...");
            }
        };
        Advice after=new AfterReturningAdvice(){
            @Override
            public void afterReturning(Object returnValue,Method method,
                                       Object[] args,Object target) throws Throwable{
                System.out.println("这里是纯底层拦截另一种实现方式——之后....");
            }
        };
        // Advice throwsAdvice = new A();
        // Advice afterAdvice = new AfterAdvice() {};
        Advice around=new MethodInterceptor(){
            @Override
            public Object invoke(MethodInvocation invocation) throws Throwable{
                System.out.println("这里是纯底层拦截另一种实现方式——MethodInterceptor——前面...");
                Object returnValue=invocation.proceed();//放行
                System.out.println("这里是纯底层拦截另一种实现方式——MethodInterceptor——之后...");
                return returnValue;
            }
        };
        //切面 = 切点 + 通知
        Advisor beforeAdvisor=new DefaultPointcutAdvisor(cut,before);
        Advisor afterAdvisor=new DefaultPointcutAdvisor(cut,after);
        Advisor aroundAdvisor=new DefaultPointcutAdvisor(cut,around);
        // Advisor throwsAdviceAdvisor = new DefaultPointcutAdvisor(cut, throwsAdvice);
        // 给代理工厂一个切面 ---注意,添加的顺序的拦截动作执行的顺序是有关系的!!!
        factory.addAdvisors(beforeAdvisor,afterAdvisor,aroundAdvisor);
        //从代理工厂中获取一个代理后的对象
        FitCoreAopClassicImpl fitCoreAopClassicImplProxyXg=(FitCoreAopClassicImpl)factory.getProxy();
        fitCoreAopClassicImplProxyXg.fitCoreAopClassicBeanInitProxyXg();
        return "MyCode";
    }
}
