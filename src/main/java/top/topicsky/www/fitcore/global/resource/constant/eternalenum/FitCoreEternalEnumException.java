package top.topicsky.www.fitcore.global.resource.constant.eternalenum;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.global.resource.constant.eternalinterface
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 22 日 17 时 42 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
public enum FitCoreEternalEnumException{
    /**
     * Arithmetic fit core eternal enum exception.
     */
    ARITHMETIC,
    /**
     * Arraystore fit core eternal enum exception.
     */
    ARRAYSTORE,
    /**
     * Classnotfound fit core eternal enum exception.
     */
    CLASSNOTFOUND,
    /**
     * Exception fit core eternal enum exception.
     */
    EXCEPTION,
    /**
     * Illegalaccess fit core eternal enum exception.
     */
    ILLEGALACCESS,
    /**
     * Illegalmonitorstate fit core eternal enum exception.
     */
    ILLEGALMONITORSTATE,
    /**
     * Illegalthreadstate fit core eternal enum exception.
     */
    ILLEGALTHREADSTATE,
    /**
     * Instantiation fit core eternal enum exception.
     */
    INSTANTIATION,
    /**
     * Negativearraysize fit core eternal enum exception.
     */
    NEGATIVEARRAYSIZE,
    /**
     * Nosuchmethod fit core eternal enum exception.
     */
    NOSUCHMETHOD,
    /**
     * Numberformat fit core eternal enum exception.
     */
    NUMBERFORMAT,
    /**
     * Runtim fit core eternal enum exception.
     */
    RUNTIM,
    /**
     * Stringindexoutofbounds fit core eternal enum exception.
     */
    STRINGINDEXOUTOFBOUNDS,
    /**
     * Unsupportedoperation fit core eternal enum exception.
     */
    UNSUPPORTEDOPERATION,
    /**
     * Arrayindexoutofbounds fit core eternal enum exception.
     */
    ARRAYINDEXOUTOFBOUNDS,
    /**
     * Classcast fit core eternal enum exception.
     */
    CLASSCAST,
    /**
     * Clonenotsupported fit core eternal enum exception.
     */
    CLONENOTSUPPORTED,
    /**
     * Enumconstantnotpresent fit core eternal enum exception.
     */
    ENUMCONSTANTNOTPRESENT,
    /**
     * Globalexceptionhandlerinter fit core eternal enum exception.
     */
    GLOBALEXCEPTIONHANDLERINTER,
    /**
     * Illegalargument fit core eternal enum exception.
     */
    ILLEGALARGUMENT,
    /**
     * Illegalstate fit core eternal enum exception.
     */
    ILLEGALSTATE,
    /**
     * Indexoutofbounds fit core eternal enum exception.
     */
    INDEXOUTOFBOUNDS,
    /**
     * Interrupted fit core eternal enum exception.
     */
    INTERRUPTED,
    /**
     * Nosuchfield fit core eternal enum exception.
     */
    NOSUCHFIELD,
    /**
     * Nullpointer fit core eternal enum exception.
     */
    NULLPOINTER,
    /**
     * Reflectiveoperation fit core eternal enum exception.
     */
    REFLECTIVEOPERATION,
    /**
     * Security fit core eternal enum exception.
     */
    SECURITY,
    /**
     * Typenotpresent fit core eternal enum exception.
     */
    TYPENOTPRESENT;
}
