package top.topicsky.www.fitcore.global.community.functionalinterfaces.topicskyconsumer;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;

import static top.topicsky.www.fitcore.system.kernel.common.consumer.ConsumerUtil.SOPL;

/**
 * 所在项目名称 ： topicskycode
 * 操作文件所在包路径　： languagefeature.functionalinterfaces.topicskyconsumer
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 07 月 03 日 10 时 09 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Component
@Transactional
public class BasicConsumerImpl implements Serializable{
    /**
     * Init predicate boolean.
     *
     * @param integer
     *         the integer
     *
     * @return the boolean
     */
/*这里就是实现过程，逻辑正常来就行，消费模式，无需返回*/
    public static void initConsumer(Integer integer){
        Boolean bl=false;
        if(2==integer) bl=true;
        else bl=false;
        SOPL.accept(bl);
    }
}
