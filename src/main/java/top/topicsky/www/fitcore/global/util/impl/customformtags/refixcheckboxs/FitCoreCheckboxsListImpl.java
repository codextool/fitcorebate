package top.topicsky.www.fitcore.global.util.impl.customformtags.refixcheckboxs;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import top.topicsky.www.fitcore.global.util.inter.customformtags.refixcheckboxs.FitCoreCheckboxsListInter;
import top.topicsky.www.fitcore.global.util.predicate.customformtags.refixcheckboxs.FitCoreCheckboxsListPredicateImpl;
import top.topicsky.www.fitcore.global.util.process.customformtags.refixcheckboxs.FitCoreCheckboxsListProcessImpl;

import java.io.Serializable;
import java.util.List;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.global.util.impl.customformtags.refixcheckboxs
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 21 日 17 时 08 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Component
@Transactional
public class FitCoreCheckboxsListImpl implements FitCoreCheckboxsListInter, Serializable{
    public <OBJ,TX,TY,TZ,TQ,TP,TS,TF,TR> TR FitCoreCheckboxsListPack(TX model,
                                                                     List<TY> listchoice,
                                                                     List<TZ> listContent,
                                                                     OBJ obj,
                                                                     TQ objProperty,
                                                                     TP choiceName,
                                                                     TS contentName,
                                                                     TF pageForword){
        FitCoreCheckboxsListProcessImpl.FitCoreCheckboxsListProcessPack(model,
                listchoice,
                obj,
                objProperty,
                choiceName,
                FitCoreCheckboxsListPredicateImpl::FitCoreCheckboxsListPredicatePack);
        FitCoreCheckboxsListProcessImpl.FitCoreCheckboxsListProcessContentPack(model,
                listContent,
                contentName,
                FitCoreCheckboxsListPredicateImpl::FitCoreCheckboxsListPredicateContentPack);
        return (TR)pageForword;
    }
}
