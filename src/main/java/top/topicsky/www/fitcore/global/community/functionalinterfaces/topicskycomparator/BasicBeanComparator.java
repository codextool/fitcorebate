package top.topicsky.www.fitcore.global.community.functionalinterfaces.topicskycomparator;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Comparator;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.global.community.functionalinterfaces.topicskycomparator
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 07 月 03 日 18 时 34 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Component
@Transactional
public class BasicBeanComparator implements Comparator<BasicBean>{
    @Override
    public int compare(BasicBean basicBean1,BasicBean basicBean2){
        if(basicBean1.getTopicskyReport()>basicBean2.getTopicskyReport())
            return -1;
        else if(basicBean1.getTopicskyReport()<basicBean2.getTopicskyReport())
            return 1;
        else{
            if(basicBean1.getTopicskyId()>basicBean2.getTopicskyId())
                return 1;
            else if(basicBean1.getTopicskyId()<basicBean2.getTopicskyId())
                return -1;
            else
                return 0;
        }
    }
}
