package top.topicsky.www.fitcore.global.exception.core;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import top.topicsky.www.fitcore.procedure.dao.core.FitCoreDaoBaseInter;
import top.topicsky.www.fitcore.procedure.dao.core.FitCoreDaoImpl;

import java.io.Serializable;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 15 日 09 时 43 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 *
 * @param <SI>
 *         the type parameter
 * @param <RT>
 *         the type parameter
 */
@Component
@Transactional
public abstract class FitCoreExceptionServiceBaseImpl<SI,RT> extends FitCoreDaoImpl implements FitCoreDaoBaseInter, Serializable{
    @Override
    public Object fitCoreExceptionCoreAdapterInit(Object o){
        return super.fitCoreExceptionCoreAdapterInit(o);
    }

    @Override
    public Object fitCoreExceptionCoreBaseInit(Object o){
        return super.fitCoreExceptionCoreBaseInit(o);
    }

    @Override
    public Object fitCoreExceptionCoreInit(Object o){
        return super.fitCoreExceptionCoreInit(o);
    }

    @Override
    public Object fitCoreExceptionControlAdapterInit(Object o){
        return super.fitCoreExceptionControlAdapterInit(o);
    }

    @Override
    public Object fitCoreExceptionControlBaseInit(Object o){
        return super.fitCoreExceptionControlBaseInit(o);
    }

    @Override
    public Object fitCoreExceptionControlInit(Object o){
        return super.fitCoreExceptionControlInit(o);
    }

    @Override
    public Object fitCoreDaoBaseInit(Object o){
        return null;
    }

    @Override
    public Object fitCoreDaoInit(Object o){
        return super.fitCoreDaoInit(o);
    }

    @Override
    public Object fitCoreControlInit(Object o){
        return o;
    }

    @Override
    public Object fitCoreInit(Object o){
        return o;
    }

    @Override
    public Object fitCoreBaseInit(Object o){
        return super.fitCoreBaseInit(o);
    }

    @Override
    public Object fitCoreAdapterInit(Object o){
        return super.fitCoreAdapterInit(o);
    }

    @Override
    public Object fitCoreControlBaseInit(Object o){
        return o;
    }

    @Override
    public Object fitCoreControlAdapterInit(Object o){
        return super.fitCoreControlAdapterInit(o);
    }

    @Override
    public Object fitCoreServiceInit(Object o){
        return super.fitCoreServiceInit(o);
    }

    @Override
    public Object fitCoreServiceBaseInit(Object o){
        return o;
    }

    @Override
    public Object fitCoreServiceAdapterInit(Object o){
        return super.fitCoreServiceAdapterInit(o);
    }
}
