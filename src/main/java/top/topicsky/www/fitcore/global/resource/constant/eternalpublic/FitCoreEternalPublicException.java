package top.topicsky.www.fitcore.global.resource.constant.eternalpublic;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;

/**
 * The type Fit core eternal public exception.
 */
@Component
@Transactional
public class FitCoreEternalPublicException implements Serializable{
    /**
     * The constant ARITHMETIC.
     */
    public static final String ARITHMETIC="ARITHMETIC(算数异常)";
    /**
     * The constant ARRAYSTORE.
     */
    public static final String ARRAYSTORE="ARRAYSTORE(集合存储异常)";
    /**
     * The constant CLASSNOTFOUND.
     */
    public static final String CLASSNOTFOUND="CLASSNOTFOUND(未匹配类异常)";
    /**
     * The constant EXCEPTION.
     */
    public static final String EXCEPTION="EXCEPTION(全局异常)";
    /**
     * The constant ILLEGALACCESS.
     */
    public static final String ILLEGALACCESS="ILLEGALACCESS(非法接入异常)";
    /**
     * The constant ILLEGALMONITORSTATE.
     */
    public static final String ILLEGALMONITORSTATE="ILLEGALMONITORSTATE(非法监视状态异常)";
    /**
     * The constant ILLEGALTHREADSTATE.
     */
    public static final String ILLEGALTHREADSTATE="ILLEGALTHREADSTATE(非法线程状态异常)";
    /**
     * The constant INSTANTIATION.
     */
    public static final String INSTANTIATION="INSTANTIATION(实例化异常)";
    /**
     * The constant NEGATIVEARRAYSIZE.
     */
    public static final String NEGATIVEARRAYSIZE="NEGATIVEARRAYSIZE(负值数组大小异常)";
    /**
     * The constant NOSUCHMETHOD.
     */
    public static final String NOSUCHMETHOD="NOSUCHMETHOD(没有所引用方法异常)";
    /**
     * The constant NUMBERFORMAT.
     */
    public static final String NUMBERFORMAT="NUMBERFORMAT(数字格式转化格式异常)";
    /**
     * The constant RUNTIM.
     */
    public static final String RUNTIM="RUNTIM(运行时异常)";
    /**
     * The constant STRINGINDEXOUTOFBOUNDS.
     */
    public static final String STRINGINDEXOUTOFBOUNDS="STRINGINDEXOUTOFBOUNDS(字符串索引越界异常)";
    /**
     * The constant UNSUPPORTEDOPERATION.
     */
    public static final String UNSUPPORTEDOPERATION="UNSUPPORTEDOPERATION(不支持的操作异常)";
    /**
     * The constant ARRAYINDEXOUTOFBOUNDS.
     */
    public static final String ARRAYINDEXOUTOFBOUNDS="ARRAYINDEXOUTOFBOUNDS(数组越界异常)";
    /**
     * The constant CLASSCAST.
     */
    public static final String CLASSCAST="CLASSCAST(类型强转异常)";
    /**
     * The constant CLONENOTSUPPORTED.
     */
    public static final String CLONENOTSUPPORTED="CLONENOTSUPPORTED(不支持类异常)";
    /**
     * The constant ENUMCONSTANTNOTPRESENT.
     */
    public static final String ENUMCONSTANTNOTPRESENT="ENUMCONSTANTNOTPRESENT(枚举常量不存在异常)";
    /**
     * The constant GLOBALEXCEPTIONHANDLERINTER.
     */
    public static final String GLOBALEXCEPTIONHANDLERINTER="GLOBALEXCEPTIONHANDLERINTER(这里是全局异常处理)";
    /**
     * The constant ILLEGALARGUMENT.
     */
    public static final String ILLEGALARGUMENT="ILLEGALARGUMENT(非法参数参数)";
    /**
     * The constant ILLEGALSTATE.
     */
    public static final String ILLEGALSTATE="ILLEGALSTATE(非法状态异常)";
    /**
     * The constant INDEXOUTOFBOUNDS.
     */
    public static final String INDEXOUTOFBOUNDS="INDEXOUTOFBOUNDS(索引越界异常)";
    /**
     * The constant INTERRUPTED.
     */
    public static final String INTERRUPTED="INTERRUPTED(异常中断)";
    /**
     * The constant NOSUCHFIELD.
     */
    public static final String NOSUCHFIELD="NOSUCHFIELD(没有这样的域异常)";
    /**
     * The constant NULLPOINTER.
     */
    public static final String NULLPOINTER="NULLPOINTER(空指针异常)";
    /**
     * The constant REFLECTIVEOPERATION.
     */
    public static final String REFLECTIVEOPERATION="REFLECTIVEOPERATION(反射操作异常)";
    /**
     * The constant SECURITY.
     */
    public static final String SECURITY="SECURITY(安全异常)";
    /**
     * The constant TYPENOTPRESENT.
     */
    public static final String TYPENOTPRESENT="TYPENOTPRESENT(类型不存在异常)";
}
