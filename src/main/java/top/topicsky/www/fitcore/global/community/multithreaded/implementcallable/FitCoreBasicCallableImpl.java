package top.topicsky.www.fitcore.global.community.multithreaded.implementcallable;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.concurrent.Callable;

import static top.topicsky.www.fitcore.system.kernel.common.consumer.ConsumerUtil.SOPL;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.global.community.multithreaded.implementcallable
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 07 月 05 日 10 时 56 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 *
 * @param <V>
 *         the type parameter
 */
@Component
@Transactional
public class FitCoreBasicCallableImpl<V> implements Callable<V>, FitCoreBasicCallableInter, Serializable{
    private Integer integerx=0;

    @Override
    public V call() throws Exception{
        SOPL.accept("这里开始一个新线程");
        for(int i=0;i<300;i++){
            synchronized(this){
                SOPL.accept(Thread.currentThread().getName()+":"+integerx);
                integerx++;
            }
        }
        return (V)integerx;
    }
}
