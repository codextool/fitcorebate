package top.topicsky.www.fitcore.global.resource.constant;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.global.resource.constant
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 16 日 19 时 57 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Component
@Transactional
public class FitCoreStaticFinalNumber implements Serializable{
    /**
     * The constant FITZERO.
     */
    public static final Integer FITZERO=0;
    /**
     * The constant FITONE.
     */
    public static final Integer FITONE=1;
    /**
     * The constant FITTWO.
     */
    public static final Integer FITTWO=2;
    /**
     * The constant FITTHREE.
     */
    public static final Integer FITTHREE=3;
    /**
     * The constant FITFOUR.
     */
    public static final Integer FITFOUR=4;
    /**
     * The constant FITFIVE.
     */
    public static final Integer FITFIVE=5;
    /**
     * The constant FITSIX.
     */
    public static final Integer FITSIX=6;
    /**
     * The constant FITSEVEN.
     */
    public static final Integer FITSEVEN=7;
    /**
     * The constant FITEUGHT.
     */
    public static final Integer FITEUGHT=8;
    /**
     * The constant FITNINE.
     */
    public static final Integer FITNINE=9;
}
