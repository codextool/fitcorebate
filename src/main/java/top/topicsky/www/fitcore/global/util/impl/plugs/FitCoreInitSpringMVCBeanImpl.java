package top.topicsky.www.fitcore.global.util.impl.plugs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import top.topicsky.www.fitcore.global.util.inter.plugs.FitCoreInitSpringMVCBeanInter;

import javax.servlet.ServletContext;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.global.util.impl
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 06 日 11 时 32 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Component
@Transactional
public class FitCoreInitSpringMVCBeanImpl implements FitCoreInitSpringMVCBeanInter, Serializable{
    @Autowired
    private ServletContext servletContext;

    @Override
    public Map<Object,Object> InitSpringMVCBean(String[] args1){
        Map<Object,Object> beanMap=new HashMap<>();
        WebApplicationContext requiredWebApplicationContext=WebApplicationContextUtils.getRequiredWebApplicationContext(servletContext);
        for(String beanName : args1){
            Object beanEntity=requiredWebApplicationContext.getBean(beanName);
            beanMap.put(beanName,beanEntity);
        }
        return beanMap;
    }

    @Override
    public Object InitSpringMVCBeanSimple(String args1){
        WebApplicationContext requiredWebApplicationContext=WebApplicationContextUtils.getRequiredWebApplicationContext(servletContext);
        return requiredWebApplicationContext.getBean(args1);
    }
}
