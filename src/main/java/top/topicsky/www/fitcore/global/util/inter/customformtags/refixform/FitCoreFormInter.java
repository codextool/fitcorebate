package top.topicsky.www.fitcore.global.util.inter.customformtags.refixform;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;

import java.io.Serializable;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.global.util.inter.customformtags.refixform
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 21 日 17 时 08 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Component
@Transactional
public interface FitCoreFormInter extends Serializable{
    /**
     * Fit core form pack string.
     *
     * @param <OBJ>
     *         the type parameter
     * @param model
     *         the model
     * @param obj
     *         the obj
     * @param objName
     *         the obj name
     * @param pageForword
     *         the page forword
     *
     * @return the string
     */
    <OBJ> String FitCoreFormPack(
            /* spring 模型*/
            Model model,
                                      /*多选框的实际值存储对象*/
            OBJ obj,
                                      /*页面Model识别名*/
            String objName,
                                       /*存页面转发值*/
            String pageForword);
}
