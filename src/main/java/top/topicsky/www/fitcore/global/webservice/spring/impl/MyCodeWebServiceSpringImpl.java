package top.topicsky.www.fitcore.global.webservice.spring.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import top.topicsky.www.fitcore.global.webservice.spring.inter.MyCodeWebServiceSpringInter;
import top.topicsky.www.fitcore.procedure.service.MycodeEntityServiceInter;

import javax.jws.WebService;
import java.io.Serializable;
/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.global.webservice.cxf.impl
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 03 日 10 时 46 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */

/**
 * Spring WebService
 * <p>
 * 关于 Spring 的 WebService 服务在 springmvc。xml 中配置
 * <p>
 * 服务实现类
 */
@Component
@Transactional
@WebService
public class MyCodeWebServiceSpringImpl implements MyCodeWebServiceSpringInter, Serializable{
    /*#####################################注入控制层######################################################*/
    /*#####################################注入控制层######################################################*/
    /*#####################################注入业务层######################################################*/
    @Qualifier("mycodeEntityServiceImpl")
    @Autowired
    private MycodeEntityServiceInter mycodeEntityServiceImpl;

    /*#####################################注入业务层######################################################*/
    /*#####################################注入持久层######################################################*/

    /*#####################################注入持久层######################################################*/
    /*#####################################注入数据层######################################################*/

    /*#####################################注入数据层######################################################*/
    /*#####################################依赖配置类######################################################*/

    /*#####################################依赖配置类######################################################*/
    /*#####################################自定义注解######################################################*/

    /*#####################################自定义注解######################################################*/

    @Override
    public String selectById(Long mycodeid){
        //MycodeEntity mycodeEntity=mycodeEntityServiceImpl.selectById(mycodeid);
        //System.out.println(mycodeEntity);
        return "MyCode";
    }
}
