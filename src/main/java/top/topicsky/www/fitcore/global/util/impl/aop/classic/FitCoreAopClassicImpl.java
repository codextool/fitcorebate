package top.topicsky.www.fitcore.global.util.impl.aop.classic;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import top.topicsky.www.fitcore.global.util.inter.aop.classic.FitCoreAopClassicInter;
import top.topicsky.www.fitcore.global.util.predicate.aop.classic.invock.FitCoreAopClassicInvockPredicateImpl;
import top.topicsky.www.fitcore.global.util.process.aop.classic.invock.FitCoreAopClassicInvockProcessImpl;

import java.io.Serializable;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.global.util.impl.aop.classic
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 07 日 19 时 30 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Component
@Transactional
public class FitCoreAopClassicImpl implements FitCoreAopClassicInter, Serializable{
    @Override
    public void fitCoreAopClassicBeanInitProxyXa(){
        Runnable runnable=()->System.out.println(
                FitCoreAopClassicInvockProcessImpl.FitCoreAopClassicInvockInit("Init Message",
                        FitCoreAopClassicInvockPredicateImpl::FitCoreAopClassicInvockInit)
        );
        runnable.run();
        System.out.println(Thread.currentThread().getStackTrace()[1].getMethodName());
    }

    @Override
    public void fitCoreAopClassicBeanInitProxyXb(){
        Runnable runnable=()->System.out.println(
                FitCoreAopClassicInvockProcessImpl.FitCoreAopClassicInvockInit("Init Message",
                        FitCoreAopClassicInvockPredicateImpl::FitCoreAopClassicInvockInit)
        );
        runnable.run();
        System.out.println(Thread.currentThread().getStackTrace()[1].getMethodName());
    }

    @Override
    public void fitCoreAopClassicBeanInitProxyXc(){
        Runnable runnable=()->System.out.println(
                FitCoreAopClassicInvockProcessImpl.FitCoreAopClassicInvockInit("Init Message",
                        FitCoreAopClassicInvockPredicateImpl::FitCoreAopClassicInvockInit)
        );
        runnable.run();
        System.out.println(Thread.currentThread().getStackTrace()[1].getMethodName());
    }

    @Override
    public void fitCoreAopClassicBeanInitProxyXd(){
        Runnable runnable=()->System.out.println(
                FitCoreAopClassicInvockProcessImpl.FitCoreAopClassicInvockInit("Init Message",
                        FitCoreAopClassicInvockPredicateImpl::FitCoreAopClassicInvockInit)
        );
        runnable.run();
        System.out.println(Thread.currentThread().getStackTrace()[1].getMethodName());
    }

    @Override
    public void fitCoreAopClassicBeanInitProxyXe(){
        Runnable runnable=()->System.out.println(
                FitCoreAopClassicInvockProcessImpl.FitCoreAopClassicInvockInit("Init Message",
                        FitCoreAopClassicInvockPredicateImpl::FitCoreAopClassicInvockInit)
        );
        runnable.run();
        System.out.println(Thread.currentThread().getStackTrace()[1].getMethodName());
    }

    @Override
    public void fitCoreAopClassicBeanInitProxyXf(){
        Runnable runnable=()->System.out.println(
                FitCoreAopClassicInvockProcessImpl.FitCoreAopClassicInvockInit("Init Message",
                        FitCoreAopClassicInvockPredicateImpl::FitCoreAopClassicInvockInit)
        );
        runnable.run();
        System.out.println(Thread.currentThread().getStackTrace()[1].getMethodName());
    }

    @Override
    public void fitCoreAopClassicBeanInitProxyXg(){
        Runnable runnable=()->System.out.println(
                FitCoreAopClassicInvockProcessImpl.FitCoreAopClassicInvockInit("Init Message",
                        FitCoreAopClassicInvockPredicateImpl::FitCoreAopClassicInvockInit)
        );
        runnable.run();
        System.out.println(Thread.currentThread().getStackTrace()[1].getMethodName());
    }

    @Override
    public void fitCoreAopClassicBeanInitProxyXh(){
        Runnable runnable=()->System.out.println(
                FitCoreAopClassicInvockProcessImpl.FitCoreAopClassicInvockInit("Init Message",
                        FitCoreAopClassicInvockPredicateImpl::FitCoreAopClassicInvockInit)
        );
        runnable.run();
        System.out.println(Thread.currentThread().getStackTrace()[1].getMethodName());
    }

    @Override
    public void fitCoreAopClassicBeanInitProxyXi(){
        Runnable runnable=()->System.out.println(
                FitCoreAopClassicInvockProcessImpl.FitCoreAopClassicInvockInit("Init Message",
                        FitCoreAopClassicInvockPredicateImpl::FitCoreAopClassicInvockInit)
        );
        runnable.run();
        System.out.println(Thread.currentThread().getStackTrace()[1].getMethodName());
    }
}
