package top.topicsky.www.fitcore.global.util.process.aop.pojo.advice;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import top.topicsky.www.fitcore.system.kernel.engine.Engine_Basic_Object_Inter;

import java.io.Serializable;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.global.util.predicate.aop.pojo.advice
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 20 日 18 时 33 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Component
@Transactional
public class FitCoreAopPojoAdviceProcessImpl implements Serializable{
    /**
     * Fit core aop pojo advice init string.
     *
     * @param initString
     *         the init string
     * @param initPredicate
     *         the init predicate
     *
     * @return the string
     */
    public static String FitCoreAopPojoAdviceInit(String initString,Engine_Basic_Object_Inter<String,String> initPredicate){
        System.out.println(initString);
        return initPredicate.fitCoreEngine(initString);
    }
}
