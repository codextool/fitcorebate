package top.topicsky.www.fitcore.global.interceptor.impl;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.context.request.WebRequest;

import java.io.Serializable;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.global.interceptor.impl
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 23 日 14 时 24 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Component
@Transactional
public class FitCoreHandlerWebRequestInterceptorInitializationAdapterImpl
        extends FitCoreHandlerWebRequestInterceptorInitializationImpl
        implements Serializable{
    private static final long serialVersionUID=7281793576223452920L;

    /**
     * Instantiates a new Fit core handler web request interceptor initialization adapter.
     */
    public FitCoreHandlerWebRequestInterceptorInitializationAdapterImpl(){
        super();
    }

    @Override
    public void FitCoreHandlerWebRequestInterceptorInitializationInit(){
        super.FitCoreHandlerWebRequestInterceptorInitializationInit();
    }

    @Override
    public void preHandle(WebRequest webRequest) throws Exception{
        super.preHandle(webRequest);
    }

    @Override
    public void postHandle(WebRequest webRequest,ModelMap modelMap) throws Exception{
        super.postHandle(webRequest,modelMap);
    }

    @Override
    public void afterCompletion(WebRequest webRequest,Exception e) throws Exception{
        super.afterCompletion(webRequest,e);
    }
}
