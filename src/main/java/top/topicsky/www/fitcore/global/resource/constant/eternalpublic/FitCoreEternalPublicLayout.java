package top.topicsky.www.fitcore.global.resource.constant.eternalpublic;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;

/**
 * The type Fit core eternal public layout.
 */
@Component
@Transactional
public class FitCoreEternalPublicLayout implements Serializable{
    /**
     * The constant LOGINJSP.
     */
    public static final String LOGINJSP="login.jsp";
    /**
     * The constant INDEXJSP.
     */
    public static final String INDEXJSP="index.jsp";
    /**
     * The constant ERRORJSP.
     */
    public static final String ERRORJSP="error.jsp";
}
