package top.topicsky.www.fitcore.global.resource.constant.eternalpublic;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;

/**
 * The type Fit core eternal public number.
 */
@Component
@Transactional
public class FitCoreEternalPublicNumber implements Serializable{
    /**
     * The constant FITZERO.
     */
    public static final String FITZERO="ZERO";
    /**
     * The constant FITONE.
     */
    public static final String FITONE="ONE";
    /**
     * The constant FITTWO.
     */
    public static final String FITTWO="TWO";
    /**
     * The constant FITTHREE.
     */
    public static final String FITTHREE="THREE";
    /**
     * The constant FITFOUR.
     */
    public static final String FITFOUR="FOUR";
    /**
     * The constant FITFIVE.
     */
    public static final String FITFIVE="FIVE";
    /**
     * The constant FITSIX.
     */
    public static final String FITSIX="SIX";
    /**
     * The constant FITSEVEN.
     */
    public static final String FITSEVEN="SEVEN";
    /**
     * The constant FITEIGHT.
     */
    public static final String FITEIGHT="EIGHT";
    /**
     * The constant FITNINE.
     */
    public static final String FITNINE="NINE";
    /**
     * The constant FITZEROC.
     */
    public static final String FITZEROC="零";
    /**
     * The constant FITONEC.
     */
    public static final String FITONEC="一";
    /**
     * The constant FITTWOC.
     */
    public static final String FITTWOC="二";
    /**
     * The constant FITTHREEC.
     */
    public static final String FITTHREEC="三";
    /**
     * The constant FITFOURC.
     */
    public static final String FITFOURC="四";
    /**
     * The constant FITFIVEC.
     */
    public static final String FITFIVEC="五";
    /**
     * The constant FITSIXC.
     */
    public static final String FITSIXC="六";
    /**
     * The constant FITSEVENC.
     */
    public static final String FITSEVENC="七";
    /**
     * The constant FITEIGHTC.
     */
    public static final String FITEIGHTC="八";
    /**
     * The constant FITNINEC.
     */
    public static final String FITNINEC="九";
}
