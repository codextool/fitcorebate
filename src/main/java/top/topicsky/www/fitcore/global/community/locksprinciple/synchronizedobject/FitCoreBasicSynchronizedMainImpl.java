package top.topicsky.www.fitcore.global.community.locksprinciple.synchronizedobject;


import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.global.community.locksprinciple.synchronizedobject
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 07 月 05 日 13 时 43 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Component
@Transactional
public class FitCoreBasicSynchronizedMainImpl implements FitCoreBasicSynchronizedMainInter, Serializable{
    /**
     * Main.
     *
     * @param args
     *         the args
     */
/* 这里涉及到内置锁的一个概念
     * 对象的内置锁和对象的状态之间是没有内在的关联的，虽然大多数类都将内置锁用做一种有效的加锁机制，但对象的域并不一定通过内置锁来保护
     * 当获取到与对象关联的内置锁时，并不能阻止其他线程访问该对象
     * 当某个线程获得对象的锁之后，只能阻止其他线程获得同一个锁
     * 之所以每个对象都有一个内置锁，是为了免去显式地创建锁对象
     * */
    public static void main(String[] args){
        Supplier<FitCoreBasicSynchronizedObjectImpl> supplierObject=()->new FitCoreBasicSynchronizedObjectImpl();
        final FitCoreBasicSynchronizedObjectImpl fitCoreBasicSynchronizedObjectImpl=supplierObject.get();
        Function<FitCoreBasicSynchronizedObjectImpl,Runnable> otri=(f)->(Runnable)()->f.initMethod();
        Function<FitCoreBasicSynchronizedObjectImpl,Runnable> otrh=(f)->(Runnable)()->f.haltMethod();
        Function<Runnable,Thread> rtf=(runnable)->new Thread(runnable);
        rtf.apply(otri.apply(fitCoreBasicSynchronizedObjectImpl)).start();
        rtf.apply(otrh.apply(fitCoreBasicSynchronizedObjectImpl)).start();
    }

    /**
     * Invock init topicsky.
     */

    public void invockInitTopicsky(){
        /*测试框架*/
        this.initTopicsky();
    }

    @Override
    public void initTopicsky(){
    }
}
