package top.topicsky.www.fitcore.global.util.process.customformtags.refixcheckbox;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import top.topicsky.www.fitcore.global.resource.constant.FitCoreStaticFinalVariable;
import top.topicsky.www.fitcore.system.kernel.engine.Engine_Basic_Object_Inter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.global.util.process.customformtags.refixcheckbox
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 21 日 17 时 08 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Component
@Transactional
public class FitCoreCheckboxProcessImpl implements Serializable{
    /**
     * Fit core checkbox process pack.
     *
     * @param <OBJ>
     *         the type parameter
     * @param <TX>
     *         the type parameter
     * @param <TY>
     *         the type parameter
     * @param <TZ>
     *         the type parameter
     * @param <TQ>
     *         the type parameter
     * @param <TP>
     *         the type parameter
     * @param <TS>
     *         the type parameter
     * @param <TF>
     *         the type parameter
     * @param <TR>
     *         the type parameter
     * @param obj
     *         the obj
     * @param objPropert
     *         the obj propert
     * @param objValue
     *         the obj value
     * @param booleanPredicate
     *         the boolean predicate
     */
    public static <OBJ,TX,TY,TZ,TQ,TP,TS,TF,TR> void FitCoreCheckboxProcessPack(OBJ obj,
                                                                                TQ objPropert,
                                                                                TP objValue,
                                                                                Engine_Basic_Object_Inter<List<TY>,TR> booleanPredicate){
        List<TY> beanObject=new ArrayList<>();
        beanObject.add((TY)obj);
        beanObject.add((TY)objPropert);
        beanObject.add((TY)objValue);
        /*被选择的可以是空，不需要判断*/
        if(FitCoreStaticFinalVariable.SUCCESS.equals(booleanPredicate.fitCoreEngine(beanObject))){
            System.out.println(FitCoreStaticFinalVariable.SUCCESS);
        }
    }

    /**
     * Fit core checkbox process content pack.
     *
     * @param <OBJ>
     *         the type parameter
     * @param <TX>
     *         the type parameter
     * @param <TY>
     *         the type parameter
     * @param <TZ>
     *         the type parameter
     * @param <TQ>
     *         the type parameter
     * @param <TP>
     *         the type parameter
     * @param <TS>
     *         the type parameter
     * @param <TF>
     *         the type parameter
     * @param <TR>
     *         the type parameter
     * @param model
     *         the model
     * @param listchoice
     *         the listchoice
     * @param listchoiceName
     *         the listchoice name
     * @param obj
     *         the obj
     * @param choiceName
     *         the choice name
     * @param contentPredicate
     *         the content predicate
     */
    public static <OBJ,TX,TY,TZ,TQ,TP,TS,TF,TR> void FitCoreCheckboxProcessContentPack(TX model,
                                                                                       List<TY> listchoice,
                                                                                       TZ listchoiceName,
                                                                                       OBJ obj,
                                                                                       TS choiceName,
                                                                                       Engine_Basic_Object_Inter<List<Object>,String> contentPredicate){
        List<Object> beanObject=new ArrayList<>();
        beanObject.add(obj);
        beanObject.add(listchoiceName);
        beanObject.add(listchoice);
        /*被选择的可以是空，不需要判断*/
        if(FitCoreStaticFinalVariable.SUCCESS.equals(contentPredicate.fitCoreEngine(beanObject))){
            ((Model)model).addAttribute((String)choiceName,obj);
            System.out.println(FitCoreStaticFinalVariable.SUCCESS);
        }
    }
}
