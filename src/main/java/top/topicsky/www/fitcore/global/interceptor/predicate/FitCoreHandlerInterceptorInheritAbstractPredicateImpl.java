package top.topicsky.www.fitcore.global.interceptor.predicate;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.global.interceptor.impl
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 23 日 14 时 24 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Component
@Transactional
public class FitCoreHandlerInterceptorInheritAbstractPredicateImpl implements Serializable{
    /**
     * Fit core handler interceptor abstract inherit init string.
     *
     * @param initAbstractString
     *         the init abstract string
     *
     * @return the string
     */
    public static String FitCoreHandlerInterceptorAbstractInheritInit(String initAbstractString){
        System.out.println(initAbstractString);
        return initAbstractString;
    }
}
