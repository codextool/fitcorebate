package top.topicsky.www.fitcore.global.util.predicate.plugs;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.List;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.global.util.inter.plugs
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 21 日 16 时 30 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Component
@Transactional
public class FitCoreBeanGetSetReflectPredicateImpl implements Serializable{
    /**
     * 该方法用于传入某实例对象以及对象方法名，通过反射调用该对象的某个get方法
     *
     * @param <TI>
     *         the type parameter
     * @param <TR>
     *         the type parameter
     * @param list
     *         the list
     *
     * @return the method
     */
    public static <TI,TR> Method getPropertyPredicate(List<TI> list){
        //此处应该判断beanObj,property不为null
        PropertyDescriptor pd=null;
        try{
            pd=new PropertyDescriptor((String)list.get(1),list.get(0).getClass());
        }catch(IntrospectionException e){
            e.printStackTrace();
        }
        if(pd.getReadMethod()==null){
        }
        return pd.getReadMethod();
    }

    ;

    /**
     * 该方法用于传入某实例对象以及对象方法名、修改值，通过放射调用该对象的某个set方法设置修改值
     *
     * @param <TI>
     *         the type parameter
     * @param <TR>
     *         the type parameter
     * @param list
     *         the list
     *
     * @return the method
     */
    public static <TI,TR> Method setPropertyPredicate(List<TI> list){
        //此处应该判断beanObj,property不为null
        PropertyDescriptor pd=null;
        try{
            pd=new PropertyDescriptor((String)list.get(1),list.get(0).getClass());
        }catch(IntrospectionException e){
            e.printStackTrace();
        }
        Method setMethod=pd.getWriteMethod();
        if(pd.getWriteMethod()==null){
        }
        return pd.getWriteMethod();
    }
}
