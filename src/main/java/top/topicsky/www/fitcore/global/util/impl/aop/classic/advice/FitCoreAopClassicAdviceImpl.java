package top.topicsky.www.fitcore.global.util.impl.aop.classic.advice;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.springframework.aop.AfterReturningAdvice;
import org.springframework.aop.MethodBeforeAdvice;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import top.topicsky.www.fitcore.global.util.inter.aop.classic.advice.FitCoreAopClassicAdviceInter;
import top.topicsky.www.fitcore.global.util.predicate.aop.classic.advice.FitCoreAopClassicAdvicePredicateImpl;
import top.topicsky.www.fitcore.global.util.process.aop.classic.advice.FitCoreAopClassicAdviceProcessImpl;

import java.io.Serializable;
import java.lang.reflect.Method;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.global.util.impl.aop.classic
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 07 日 19 时 32 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
/*
* Spring支持五种类型的通知：
* Before(前)  org.apringframework.aop.MethodBeforeAdvice
* After-returning(返回后) org.springframework.aop.AfterReturningAdvice
* After-throwing(抛出后) org.springframework.aop.ThrowsAdvice
* Arround(周围) org.aopaliance.intercept.MethodInterceptor
* Introduction(引入) org.springframework.aop.IntroductionInterceptor
*
* MethodBeforeAdvice
* AfterReturningAdvice
* MethodInterceptor
* 目标方法之前的拦截的优先级表现：优先级越高，越先拦截
* 目标方法之后的拦截的优先级表现：优先级越高，越后拦截
* 在xml文件中，ProxyFactoryBean.interceptorNames配置顺序决定advice的执行顺序
* */
@Component
@Transactional
public class FitCoreAopClassicAdviceImpl implements MethodBeforeAdvice, AfterReturningAdvice, MethodInterceptor, FitCoreAopClassicAdviceInter, Serializable{
    @Override
    public void initAfter() throws Throwable{
        Runnable runnable=()->System.out.println(
                FitCoreAopClassicAdviceProcessImpl.FitCoreAopClassicAdviceInit("Init Message",
                        FitCoreAopClassicAdvicePredicateImpl::FitCoreAopClassicAdviceInit)
        );
        runnable.run();
        System.out.println(Thread.currentThread().getStackTrace()[1].getMethodName());
    }

    @Override
    public void initBefore() throws Throwable{
        Runnable runnable=()->System.out.println(
                FitCoreAopClassicAdviceProcessImpl.FitCoreAopClassicAdviceInit("Init Message",
                        FitCoreAopClassicAdvicePredicateImpl::FitCoreAopClassicAdviceInit)
        );
        runnable.run();
        System.out.println(Thread.currentThread().getStackTrace()[1].getMethodName());
    }

    @Override
    public void afterReturning(Object o,Method method,Object[] objects,Object o1) throws Throwable{
        Runnable runnable=()->System.out.println(
                FitCoreAopClassicAdviceProcessImpl.FitCoreAopClassicAdviceInit("Init Message",
                        FitCoreAopClassicAdvicePredicateImpl::FitCoreAopClassicAdviceInit)
        );
        runnable.run();
        System.out.println(Thread.currentThread().getStackTrace()[1].getMethodName());
    }

    @Override
    public void before(Method method,Object[] objects,Object o) throws Throwable{
        Runnable runnable=()->System.out.println(
                FitCoreAopClassicAdviceProcessImpl.FitCoreAopClassicAdviceInit("Init Message",
                        FitCoreAopClassicAdvicePredicateImpl::FitCoreAopClassicAdviceInit)
        );
        runnable.run();
        System.out.println(Thread.currentThread().getStackTrace()[1].getMethodName());
    }

    @Override
    public Object invoke(MethodInvocation methodInvocation) throws Throwable{
        Runnable runnable=()->System.out.println(
                FitCoreAopClassicAdviceProcessImpl.FitCoreAopClassicAdviceInit("Init Message",
                        FitCoreAopClassicAdvicePredicateImpl::FitCoreAopClassicAdviceInit)
        );
        runnable.run();
        System.out.println("前面拦截....");
        Object resObj=methodInvocation.proceed();
        System.out.println("后面拦截.....");
        System.out.println(Thread.currentThread().getStackTrace()[1].getMethodName());
        return resObj;
    }
}
