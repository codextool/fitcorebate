package top.topicsky.www.fitcore.global.dto;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import top.topicsky.www.fitcore.global.entity.FitCoreUserEntity;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.global.dto
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 14 日 10 时 27 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */

/**
 * 使用 @Data 相当于同时使用了@ToString、@EqualsAndHashCode、@Getter、@Setter和@RequiredArgsConstructor这些注解
 *
 * @Cleanup 他可以帮我们在需要释放的资源位置自动加上释放代码
 * @Synchronized 可以帮我们在方法上添加同步代码块
 * @XmlRootElement 定义了注解注入实例
 */
@Getter(AccessLevel.PUBLIC)
@Setter(AccessLevel.PUBLIC)
@ToString(callSuper=true)
/**
 * 这里如果不写false,会报错,
 * Error:(21, 1) 警告: Generating equals/hashCode implementation but without a call to superclass,
 * even though this class does not extend java.lang.Object. If this is intentional,
 * add '@EqualsAndHashCode(callSuper=false)' to your type.
 */
@EqualsAndHashCode(callSuper=false)
@RequiredArgsConstructor
@AllArgsConstructor
@Slf4j
@Repository
@Transactional
@XmlRootElement
public class FitCoreUserIntegrateDto implements Serializable{
    private FitCoreUserEntity fitCoreUserEntity;
    private String fitCoreParam;
    private String fitCoreValue;
}
