package top.topicsky.www.fitcore.global.cache.redis.advanced.core;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import java.util.concurrent.locks.ReentrantLock;

import static top.topicsky.www.fitcore.system.kernel.common.consumer.ConsumerUtil.SOPL;

/**
 * 所在项目名称 ： fitcorebate
 * 操作文件所在包路径　： top.topicsky.www.fitcore.global.cache.redis.advanced.core
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 07 月 21 日 16 时 50 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
public class FitCoreAdvancedReentrantLockImpl{
    /**
     * 这是多线程池
     */
    protected static ReentrantLock lockPool=new ReentrantLock();
    /**
     * 这是同步redis服务
     */
    protected static ReentrantLock lockJedis=new ReentrantLock();
    /**
     * Redis服务器IP，可以用 , 隔开，第二个是备用地址
     */
    private static String ADDR_ARRAY="192.168.196.100";
    /**
     * Redis的端口号，默认6379
     */
    private static int PORT=6379;
    /**
     * Redis服务器访问密码
     */
    private static String AUTH="root";
    /**
     * 可用连接实例的最大数目，默认值为8
     * 如果赋值为-1，则表示不限制
     * 如果pool已经分配了maxActive个jedis实例
     * 则此时pool的状态为exhausted(耗尽)
     */
    private static int MAX_ACTIVE=8;
    /**
     * 控制一个pool最多有多少个状态为idle(空闲的)的jedis实例，默认值也是8
     */
    private static int MAX_IDLE=8;
    /**
     * 等待可用连接的最大时间，单位毫秒，默认值为-1，表示永不超时
     * 如果超过等待时间，则直接抛出JedisConnectionException
     */
    private static int MAX_WAIT=3000;
    /**
     * 超时时间
     */
    private static int TIMEOUT=10000;
    /**
     * 在borrow一个jedis实例时，是否提前进行validate操作
     * 如果为true，则得到的jedis实例均是可用的
     */
    private static boolean TEST_ON_BORROW=false;
    private static JedisPool jedisPool=null;
    /**
     * redis过期时间,以秒为单位
     */
    public final static int EXRP_HOUR=60*60;            //一小时
    /**
     * The constant EXRP_DAY.
     */
    public final static int EXRP_DAY=60*60*24;        //一天
    /**
     * The constant EXRP_MONTH.
     */
    public final static int EXRP_MONTH=60*60*24*30; //一个月

    /**
     * 初始化Redis连接池
     */
    private static void initialPool(){
        try{
            /**
             * 建立redis连接配置
             * */
            JedisPoolConfig config=new JedisPoolConfig();
            config.setMaxTotal(MAX_ACTIVE);
            config.setMaxIdle(MAX_IDLE);
            config.setMaxWaitMillis(MAX_WAIT);
            config.setTestOnBorrow(TEST_ON_BORROW);
            jedisPool=new JedisPool(config,ADDR_ARRAY.split(",")[0],PORT,TIMEOUT,AUTH);
            SOPL.accept("建立redis服务端成功");
        }catch(Exception e){
            SOPL.accept("建立的首个redis服务端失败 : "+e);
            try{
                /**
                 * 如果第一个IP异常，则访问第二个IP
                 */
                JedisPoolConfig config=new JedisPoolConfig();
                config.setMaxTotal(MAX_ACTIVE);
                config.setMaxIdle(MAX_IDLE);
                config.setMaxWaitMillis(MAX_WAIT);
                config.setTestOnBorrow(TEST_ON_BORROW);
                jedisPool=new JedisPool(config,ADDR_ARRAY.split(",")[1],PORT,TIMEOUT,AUTH);
            }catch(Exception e2){
                SOPL.accept("备用地址建立redis服务端失败 : "+e2);
            }
        }
    }

    /**
     * 在多线程环境同步初始化
     */
    private static void poolInit(){
        lockPool.lock();
        try{
            if(jedisPool==null){
                initialPool();
                SOPL.accept("初始化Redis连接池");
            }
        }catch(Exception e){
            e.printStackTrace();
            SOPL.accept("初始化Redis连接池失败");
        }finally{
            lockPool.unlock();
        }
    }

    /**
     * 在多线程环境同步初始化
     *
     * @return the jedis
     */
    public static Jedis getJedis(){
        lockJedis.lock();
        if(jedisPool==null){
            poolInit();
            SOPL.accept("初始化多线程连接池");
        }
        Jedis jedis=null;
        try{
            if(jedisPool!=null){
                jedis=jedisPool.getResource();
                SOPL.accept("取得redis操作对象");
            }
        }catch(Exception e){
            SOPL.accept("取得redis操作对象失败 : "+e);
        }finally{
            returnResource(jedis);
            lockJedis.unlock();
        }
        return jedis;
    }

    /**
     * 释放jedis资源
     *
     * @param jedis
     *         the jedis
     */
    public static void returnResource(final Jedis jedis){
        if(jedis!=null&&jedisPool!=null){
            jedisPool.returnResource(jedis);
            SOPL.accept("释放jedis资源");
        }
    }

    /**
     * Set string.
     *
     * @param key
     *         the key
     * @param value
     *         the value
     */
    public synchronized static void setString(String key,String value){
        try{
            SOPL.accept("写入值 key："+key+",value："+value);
            value=StringUtils.isEmpty(value)?"":value;
            getJedis().set(key,value);
            SOPL.accept("写入值成功");
        }catch(Exception e){
            SOPL.accept("写入值失败 : "+e);
        }
    }

    /**
     * 设置 过期时间
     *
     * @param key
     *         the key
     * @param seconds
     *         the seconds
     * @param value
     *         the value
     */
    public synchronized static void setString(String key,int seconds,String value){
        try{
            SOPL.accept("写入过期时间 key："+key+",seconds："+seconds+",value"+value);
            value=StringUtils.isEmpty(value)?"":value;
            getJedis().setex(key,seconds,value);
            SOPL.accept("写入过期时间成功");
        }catch(Exception e){
            SOPL.accept("写入过期时间失败 : "+e);
        }
    }

    /**
     * 获取String值
     *
     * @param key
     *         the key
     *
     * @return the string
     */
    public synchronized static String getString(String key){
        if(getJedis()==null||!getJedis().exists(key)){
            return null;
        }
        return getJedis().get(key);
    }
}
