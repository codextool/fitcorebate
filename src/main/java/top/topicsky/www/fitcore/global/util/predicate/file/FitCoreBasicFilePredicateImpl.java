package top.topicsky.www.fitcore.global.util.predicate.file;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.*;
import java.util.Arrays;

import static top.topicsky.www.fitcore.global.resource.constant.eternalpublic.FitCoreEternalPublicExtensionName.*;
import static top.topicsky.www.fitcore.system.kernel.common.consumer.ConsumerUtil.SOPL;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.global.util.predicate.file
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 07 月 04 日 15 时 36 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Component
@Transactional
public class FitCoreBasicFilePredicateImpl implements Serializable{
    /**
     * List predicate.
     *
     * @param path
     *         the path
     */
    public static void listPredicate(File path){
        if(!path.exists()){
            SOPL.accept("文件名称不存在!");
        }else{
            if(path.isFile()){
                // 文件格式
                if(path.getName().toLowerCase().endsWith(FITPDF)
                        ||path.getName().toLowerCase().endsWith(FITDOC)
                        ||path.getName().toLowerCase().endsWith(FITCHM)
                        ||path.getName().toLowerCase().endsWith(FITHTML)
                        ||path.getName().toLowerCase().endsWith(FITHTM)){
                    SOPL.accept(path);
                    SOPL.accept(path.getName());
                }
            }else{
                Arrays.stream(path.listFiles()).forEach((f)->listPredicate(f));
            }
        }
    }

    /**
     * Copy predicate.
     *
     * @param files
     *         the files
     */
    public static void copyPredicate(File[] files){
        File tarpath=new File(files[1],files[0].getName());
        if(files[0].isDirectory()){
            tarpath.mkdir();
            File[] dir=files[0].listFiles();
            Arrays.stream(files[0].listFiles()).forEach((d)->{
                        File[] filesTemp={d,tarpath};
                        copyPredicate(filesTemp);
                    }
            );
        }else{
            try{
                // 用于读取文件的原始字节流
                InputStream is=new FileInputStream(files[0]);
                // 用于写入文件的原始字节的流
                OutputStream os=new FileOutputStream(tarpath);
                // 存储读取数据的缓冲区大小
                byte[] buf=new byte[1024];
                int len=0;
                while((len=is.read(buf))!=-1){
                    os.write(buf,0,len);
                }
                is.close();
                os.close();
            }catch(FileNotFoundException e){
                e.printStackTrace();
            }catch(IOException e){
                e.printStackTrace();
            }
        }
    }
}
