package top.topicsky.www.fitcore.global.resource.constant.eternalprivate;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.global.resource.constant.eternalinterface
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 22 日 17 时 42 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Getter(AccessLevel.PUBLIC)
@Setter(AccessLevel.PUBLIC)
@Component
@Transactional
public class FitCoreEternalPrivateException implements Serializable{
    private static final String ARITHMETIC="ARITHMETIC(算数异常)";
    private static final String ARRAYSTORE="ARRAYSTORE(集合存储异常)";
    private static final String CLASSNOTFOUND="CLASSNOTFOUND(未匹配类异常)";
    private static final String EXCEPTION="EXCEPTION(全局异常)";
    private static final String ILLEGALACCESS="ILLEGALACCESS(非法接入异常)";
    private static final String ILLEGALMONITORSTATE="ILLEGALMONITORSTATE(非法监视状态异常)";
    private static final String ILLEGALTHREADSTATE="ILLEGALTHREADSTATE(非法线程状态异常)";
    private static final String INSTANTIATION="INSTANTIATION(实例化异常)";
    private static final String NEGATIVEARRAYSIZE="NEGATIVEARRAYSIZE(负值数组大小异常)";
    private static final String NOSUCHMETHOD="NOSUCHMETHOD(没有所引用方法异常)";
    private static final String NUMBERFORMAT="NUMBERFORMAT(数字格式转化格式异常)";
    private static final String RUNTIM="RUNTIM(运行时异常)";
    private static final String STRINGINDEXOUTOFBOUNDS="STRINGINDEXOUTOFBOUNDS(字符串索引越界异常)";
    private static final String UNSUPPORTEDOPERATION="UNSUPPORTEDOPERATION(不支持的操作异常)";
    private static final String ARRAYINDEXOUTOFBOUNDS="ARRAYINDEXOUTOFBOUNDS(数组越界异常)";
    private static final String CLASSCAST="CLASSCAST(类型强转异常)";
    private static final String CLONENOTSUPPORTED="CLONENOTSUPPORTED(不支持类异常)";
    private static final String ENUMCONSTANTNOTPRESENT="ENUMCONSTANTNOTPRESENT(枚举常量不存在异常)";
    private static final String GLOBALEXCEPTIONHANDLERINTER="GLOBALEXCEPTIONHANDLERINTER(这里是全局异常处理)";
    private static final String ILLEGALARGUMENT="ILLEGALARGUMENT(非法参数参数)";
    private static final String ILLEGALSTATE="ILLEGALSTATE(非法状态异常)";
    private static final String INDEXOUTOFBOUNDS="INDEXOUTOFBOUNDS(索引越界异常)";
    private static final String INTERRUPTED="INTERRUPTED(异常中断)";
    private static final String NOSUCHFIELD="NOSUCHFIELD(没有这样的域异常)";
    private static final String NULLPOINTER="NULLPOINTER(空指针异常)";
    private static final String REFLECTIVEOPERATION="REFLECTIVEOPERATION(反射操作异常)";
    private static final String SECURITY="SECURITY(安全异常)";
    private static final String TYPENOTPRESENT="TYPENOTPRESENT(类型不存在异常)";
}
