package top.topicsky.www.fitcore.global.util.process.plugs;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import top.topicsky.www.fitcore.global.util.inter.plugs.FitCoreCookieURLEncoderInitInter;

import javax.servlet.http.Cookie;
import java.io.Serializable;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.global.util.process.plugs
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 13 日 14 时 26 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Component
@Transactional
public class FitCoreCookieURLEncoderProcessImpl implements Serializable{
    /**
     * Fit core cookie url encoder process cookie [ ].
     *
     * @param cookie
     *         the cookie
     * @param codeType
     *         the code type
     * @param encodeCookiePredicate
     *         the encode cookie predicate
     *
     * @return the cookie [ ]
     */
    public static Cookie[] FitCoreCookieURLEncoderProcess(Cookie[] cookie,String codeType,FitCoreCookieURLEncoderInitInter encodeCookiePredicate){
        for(int count=0;count<cookie.length;count++){
            String preArrayTemp=cookie[count].getValue();
            String[] curArrayTemp=preArrayTemp.split("==");
            if(curArrayTemp.length==2){
                String cookieEncoder=encodeCookiePredicate.Cookiecoder(codeType,curArrayTemp[0],curArrayTemp[1]);
                cookie[count].setValue(cookieEncoder);
            }
        }
        return cookie;
    }

    /**
     * Fit core cookie url decoder process cookie [ ].
     *
     * @param cookie
     *         the cookie
     * @param codeType
     *         the code type
     * @param decodeCookiePredicate
     *         the decode cookie predicate
     *
     * @return the cookie [ ]
     */
    public static Cookie[] FitCoreCookieURLDecoderProcess(Cookie[] cookie,String codeType,FitCoreCookieURLEncoderInitInter decodeCookiePredicate){
        for(int count=0;count<cookie.length;count++){
            String preArrayTemp=cookie[count].getValue();
            String[] curArrayTemp=preArrayTemp.split("==");
            if(curArrayTemp.length==2){
                String cookieEncoder=decodeCookiePredicate.Cookiecoder(codeType,curArrayTemp[0],curArrayTemp[1]);
                cookie[count].setValue(cookieEncoder);
            }
        }
        return cookie;
    }
}
