package top.topicsky.www.fitcore.global.util.inter.plugs;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.Map;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.global.util.inter
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 06 日 11 时 31 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Component
@Transactional
public interface FitCoreInitSpringMVCBeanInter extends Serializable{
    /**
     * Init spring mvc bean map.
     *
     * @param args1
     *         the args 1
     *
     * @return the map
     */
/*
    * request 取得 SpringMVC 上下文环境
    * args1 bean 的配置名
    **/
    public Map<Object,Object> InitSpringMVCBean(String[] args1);

    /**
     * Init spring mvc bean simple object.
     *
     * @param args1
     *         the args 1
     *
     * @return the object
     */
/*
    * request 取得 SpringMVC 上下文环境
    * args1 bean 的配置名
    **/
    public Object InitSpringMVCBeanSimple(String args1);
}
