package top.topicsky.www.fitcore.global.exception.impl;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.Serializable;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.global.exception.impl
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 19 日 13 时 15 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Component
@Transactional
public class FitCoreCloneNotSupportedExceptionAdapterImpl
        extends FitCoreCloneNotSupportedExceptionImpl
        implements Serializable{
    private static final long serialVersionUID=6344520160412602473L;

    /**
     * Instantiates a new Fit core clone not supported exception adapter.
     */
    public FitCoreCloneNotSupportedExceptionAdapterImpl(){
        super();
    }

    /**
     * Instantiates a new Fit core clone not supported exception adapter.
     *
     * @param s
     *         the s
     */
    public FitCoreCloneNotSupportedExceptionAdapterImpl(String s){
        super(s);
    }

    @Override
    public String getMessage(){
        return super.getMessage();
    }

    @Override
    public String getLocalizedMessage(){
        return super.getLocalizedMessage();
    }

    @Override
    public synchronized Throwable getCause(){
        return super.getCause();
    }

    @Override
    public synchronized Throwable initCause(Throwable cause){
        return super.initCause(cause);
    }

    @Override
    public String toString(){
        return super.toString();
    }

    @Override
    public void printStackTrace(){
        super.printStackTrace();
    }

    @Override
    public void printStackTrace(PrintStream s){
        super.printStackTrace(s);
    }

    @Override
    public void printStackTrace(PrintWriter s){
        super.printStackTrace(s);
    }

    @Override
    public synchronized Throwable fillInStackTrace(){
        return super.fillInStackTrace();
    }

    @Override
    public StackTraceElement[] getStackTrace(){
        return super.getStackTrace();
    }

    @Override
    public void setStackTrace(StackTraceElement[] stackTrace){
        super.setStackTrace(stackTrace);
    }
}
