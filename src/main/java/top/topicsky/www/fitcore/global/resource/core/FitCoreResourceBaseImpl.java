package top.topicsky.www.fitcore.global.resource.core;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 15 日 09 时 43 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 *
 * @param <SI>
 *         the type parameter
 * @param <RT>
 *         the type parameter
 */
@Component
@Transactional
public abstract class FitCoreResourceBaseImpl<SI,RT> extends FitCoreResourceImpl implements FitCoreResourceBaseInter, Serializable{
    @Override
    public Object fitCoreInit(Object o){
        return super.fitCoreInit(o);
    }

    @Override
    public Object fitCoreBaseInit(Object o){
        return super.fitCoreBaseInit(o);
    }

    @Override
    public Object fitCoreAdapterInit(Object o){
        return super.fitCoreAdapterInit(o);
    }

    @Override
    public Object fitCoreResourceInit(Object o){
        return super.fitCoreResourceInit(o);
    }

    @Override
    public Object fitCoreResourceBaseInit(Object o){
        return null;
    }
}
