package top.topicsky.www.fitcore.system.kernel.meta.custom;

import top.topicsky.www.fitcore.system.kernel.meta.custom.invock.FitCoreUserInvock;
import top.topicsky.www.fitcore.system.kernel.meta.custom.model.FitCoreUserImpl;

import static top.topicsky.www.fitcore.system.kernel.common.consumer.ConsumerUtil.SOPL;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.system.kernel.meta.custom
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 07 月 07 日 14 时 15 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
public class FitCoreCustomTest{
    /**
     * The entry point of application.
     *
     * @param args
     *         the input arguments
     *
     * @throws Exception
     *         the exception
     */
    public static void main(String[] args) throws Exception{
        FitCoreUserImpl stu=new FitCoreUserImpl();
        stu.setId("ccc");
        stu.setName("姓名");
        stu.setAge(18);
        stu.setSex("男");
        SOPL.accept(FitCoreUserInvock.save(stu));
        SOPL.accept(FitCoreUserInvock.update(stu));
    }
}
