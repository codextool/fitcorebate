package top.topicsky.www.fitcore.system.kernel.meta.original.xconstant.xpublic.model;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import top.topicsky.www.fitcore.system.kernel.meta.original.xconstant.xpublic.core.FCExtensionNamePublic;

import java.io.Serializable;

import static top.topicsky.www.fitcore.global.resource.constant.eternalpublic.FitCoreEternalPublicExtensionName.*;

/**
 * The type Fc extensionName model public.
 */
@Getter(AccessLevel.PUBLIC)
@Setter(AccessLevel.PUBLIC)
@Component
@Transactional
public class FCExtensionNameModelPublic implements Serializable{
    @FCExtensionNamePublic
    private String extensionNameDefault;
    @FCExtensionNamePublic(extensionNameExp=FITPDF)
    private String extensionNameFITPDF;
    @FCExtensionNamePublic(extensionNameExp=FITTXT)
    private String extensionNameFITTXT;
    @FCExtensionNamePublic(extensionNameExp=FITDOC)
    private String extensionNameFITDOC;
    @FCExtensionNamePublic(extensionNameExp=FITBMP)
    private String extensionNameFITBMP;
    @FCExtensionNamePublic(extensionNameExp=FITHTML)
    private String extensionNameFITHTML;
    @FCExtensionNamePublic(extensionNameExp=FITHTM)
    private String extensionNameFITHTM;
    @FCExtensionNamePublic(extensionNameExp=FITCHM)
    private String extensionNameFITCHM;
    @FCExtensionNamePublic(extensionNameExp=FITJPG)
    private String extensionNameFITJPG;
    @FCExtensionNamePublic(extensionNameExp=FITJPGE)
    private String extensionNameFITJPGE;
    @FCExtensionNamePublic(extensionNameExp=FITPNG)
    private String extensionNameFITPNG;
    @FCExtensionNamePublic(extensionNameExp=FITJS)
    private String extensionNameFITJS;
    @FCExtensionNamePublic(extensionNameExp=FITJAVA)
    private String extensionNameFITJAVA;
    @FCExtensionNamePublic(extensionNameExp=FITC)
    private String extensionNameFITC;
    @FCExtensionNamePublic(extensionNameExp=FITRB)
    private String extensionNameFITRB;
    private String extensionNameNoMetaFITPDF;
    private String extensionNameNoMetaFITTXT;
    private String extensionNameNoMetaFITDOC;
    private String extensionNameNoMetaFITBMP;
    private String extensionNameNoMetaFITHTML;
    private String extensionNameNoMetaFITHTM;
    private String extensionNameNoMetaFITCHM;
    private String extensionNameNoMetaFITJPG;
    private String extensionNameNoMetaFITJPGE;
    private String extensionNameNoMetaFITPNG;
    private String extensionNameNoMetaFITJS;
    private String extensionNameNoMetaFITJAVA;
    private String extensionNameNoMetaFITC;
    private String extensionNameNoMetaFITRB;

    @Override
    public String toString(){
        return "FCExtensionNameModelPublic{"+
                "extensionNameDefault='"+extensionNameDefault+'\''+
                ", extensionNameFITPDF='"+extensionNameFITPDF+'\''+
                ", extensionNameFITTXT='"+extensionNameFITTXT+'\''+
                ", extensionNameFITDOC='"+extensionNameFITDOC+'\''+
                ", extensionNameFITBMP='"+extensionNameFITBMP+'\''+
                ", extensionNameFITHTML='"+extensionNameFITHTML+'\''+
                ", extensionNameFITHTM='"+extensionNameFITHTM+'\''+
                ", extensionNameFITCHM='"+extensionNameFITCHM+'\''+
                ", extensionNameFITJPG='"+extensionNameFITJPG+'\''+
                ", extensionNameFITJPGE='"+extensionNameFITJPGE+'\''+
                ", extensionNameFITPNG='"+extensionNameFITPNG+'\''+
                ", extensionNameFITJS='"+extensionNameFITJS+'\''+
                ", extensionNameFITJAVA='"+extensionNameFITJAVA+'\''+
                ", extensionNameFITC='"+extensionNameFITC+'\''+
                ", extensionNameFITRB='"+extensionNameFITRB+'\''+
                ", extensionNameNoMetaFITPDF='"+extensionNameNoMetaFITPDF+'\''+
                ", extensionNameNoMetaFITTXT='"+extensionNameNoMetaFITTXT+'\''+
                ", extensionNameNoMetaFITDOC='"+extensionNameNoMetaFITDOC+'\''+
                ", extensionNameNoMetaFITBMP='"+extensionNameNoMetaFITBMP+'\''+
                ", extensionNameNoMetaFITHTML='"+extensionNameNoMetaFITHTML+'\''+
                ", extensionNameNoMetaFITHTM='"+extensionNameNoMetaFITHTM+'\''+
                ", extensionNameNoMetaFITCHM='"+extensionNameNoMetaFITCHM+'\''+
                ", extensionNameNoMetaFITJPG='"+extensionNameNoMetaFITJPG+'\''+
                ", extensionNameNoMetaFITJPGE='"+extensionNameNoMetaFITJPGE+'\''+
                ", extensionNameNoMetaFITPNG='"+extensionNameNoMetaFITPNG+'\''+
                ", extensionNameNoMetaFITJS='"+extensionNameNoMetaFITJS+'\''+
                ", extensionNameNoMetaFITJAVA='"+extensionNameNoMetaFITJAVA+'\''+
                ", extensionNameNoMetaFITC='"+extensionNameNoMetaFITC+'\''+
                ", extensionNameNoMetaFITRB='"+extensionNameNoMetaFITRB+'\''+
                '}';
    }
}
