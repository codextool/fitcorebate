package top.topicsky.www.fitcore.system.kernel.engine;

import java.io.Serializable;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.global.util.inter.engine
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 14 日 11 时 57 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 *
 * @param <RT>
 *         the type parameter
 */
@FunctionalInterface
public interface Engine_Basic_NoParam_Inter<RT> extends Serializable{
    /**
     * Fit core engine rt.
     *
     * @return the rt
     */
    RT fitCoreEngine();
}
