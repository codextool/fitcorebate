package top.topicsky.www.fitcore.system.kernel.engine.binaryoperator;

import java.io.Serializable;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.system.kernel.engine.binaryoperator
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 27 日 12 时 55 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 *
 * @param <SIA>
 *         the type parameter
 * @param <SIB>
 *         the type parameter
 * @param <RT>
 *         the type parameter
 */
@FunctionalInterface
public interface FitCoreBasicBinaryOperatorInter<SIA,SIB,RT> extends
        FitCoreDafaultBinaryOperatorInter,
        FitCoreStaticBinaryOperatorInter,
        Serializable{
    /**
     * Apply rt.
     *
     * @param sia
     *         the sia
     * @param sib
     *         the sib
     *
     * @return the rt
     */
    RT apply(SIA sia,SIB sib);

    @Override
    default void initVoidDafault() throws Exception{
    }

    @Override
    default <SI,RT> RT initBasicDafault(SI si) throws Exception{
        return null;
    }

    @Override
    default <SI,RT> RT initNoParamDafault() throws Exception{
        return null;
    }

    @Override
    default <SI> void initNoReturnDafault(SI si) throws Exception{
    }
}
