package top.topicsky.www.fitcore.system.kernel.meta.attempt.model;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import top.topicsky.www.fitcore.system.kernel.meta.attempt.core.FitCoreFruitColorMeta;
import top.topicsky.www.fitcore.system.kernel.meta.attempt.core.FitCoreFruitNameMeta;
import top.topicsky.www.fitcore.system.kernel.meta.attempt.core.FitCoreFruitProviderMeta;
import top.topicsky.www.fitcore.system.kernel.meta.attempt.source.FitCoreColor;

import java.io.Serializable;

import static top.topicsky.www.fitcore.system.kernel.common.consumer.ConsumerUtil.SOPL;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.system.kernel.meta.attempt
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 07 月 07 日 11 时 08 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Getter(AccessLevel.PUBLIC)
@Setter(AccessLevel.PUBLIC)
@Component
@Transactional
public class FitCoreAppleImpl implements Serializable{
    @FitCoreFruitNameMeta("苹果")
    private String appleName;
    @FitCoreFruitColorMeta(fruitColor=FitCoreColor.RED)
    private String appleColor;
    @FitCoreFruitProviderMeta(id=0, name="深藏彼岸社区", address="www.topicsky.top")
    private String appleProvider;

    /**
     * Display name.
     */
    public void displayName(){
        SOPL.accept("水果的名字是：苹果");
    }

    @Override
    public String toString(){
        return "类型："
                +
                appleName
                +
                "，颜色："
                +
                appleColor
                +
                "，供应商信息："
                +
                appleProvider;
    }
}
