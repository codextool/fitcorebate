package top.topicsky.www.fitcore.system.kernel.engine.comparator;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;

import static top.topicsky.www.fitcore.global.resource.constant.eternalenum.FitCoreEternalEnumVariable.*;
import static top.topicsky.www.fitcore.system.kernel.common.consumer.ConsumerUtil.SOPLOUT;
import static top.topicsky.www.fitcore.system.kernel.common.function.FunctionUtil.OTS;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.system.kernel.engine.comparator
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 27 日 11 时 34 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Component
@Transactional
public interface FitCoreStaticComparatorInter extends Serializable{
    /**
     * Init void static.
     *
     * @throws Exception
     *         the exception
     */
    static void initVoidStatic() throws Exception{
        SOPLOUT.accept(INITVOIDSTATIC);
    }

    /**
     * Init basic static rt.
     *
     * @param <SI>
     *         the type parameter
     * @param <RT>
     *         the type parameter
     * @param si
     *         the si
     *
     * @return the rt
     *
     * @throws Exception
     *         the exception
     */
    static <SI,RT> RT initBasicStatic(SI si) throws Exception{
        SOPLOUT.accept(INITBASICSTATIC);
        return (RT)si;
    }

    /**
     * Init no param static rt.
     *
     * @param <SI>
     *         the type parameter
     * @param <RT>
     *         the type parameter
     *
     * @return the rt
     *
     * @throws Exception
     *         the exception
     */
    static <SI,RT> RT initNoParamStatic() throws Exception{
        SOPLOUT.accept(INITNOPARAMSTATIC);
        return (RT)OTS.apply(SUCCESS);
    }

    /**
     * Init no return static.
     *
     * @param <SI>
     *         the type parameter
     * @param si
     *         the si
     *
     * @throws Exception
     *         the exception
     */
    static <SI> void initNoReturnStatic(SI si) throws Exception{
        SOPLOUT.accept(INITNORETURNSTATIC);
    }
}
