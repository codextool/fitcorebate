package top.topicsky.www.fitcore.system.kernel.modellibrary.behavioral.level.observer;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.Enumeration;
import java.util.Vector;

import static top.topicsky.www.fitcore.system.kernel.common.consumer.ConsumerUtil.SOPL;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.system.kernel.modellibrary.behavioral.level.observer
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 29 日 14 时 08 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Component
@Transactional
public abstract class FitCoreBasicObserverSubjectAbstractImpl implements FitCoreBasicObserverSubjectInter, Serializable{
    private Vector<FitCoreBasicObserverInter> vector=new Vector<FitCoreBasicObserverInter>();

    @Override
    public void addObserver(FitCoreBasicObserverInter observer){
        vector.add(observer);
        SOPL.accept("增加一个需要被侦测观察的实体，名称："+observer.getClass().getSimpleName());
    }

    @Override
    public void delObserver(FitCoreBasicObserverInter observer){
        vector.remove(observer);
        SOPL.accept("删除一个需要被侦测观察的实体，名称："+observer.getClass().getSimpleName());
    }

    @Override
    public void notifyObservers(){
        SOPL.accept("通知所有的被侦测观察的实体开始");
        Enumeration<FitCoreBasicObserverInter> enumo=vector.elements();
        while(enumo.hasMoreElements()){
            FitCoreBasicObserverInter fitCoreBasicObserverInter=enumo.nextElement();
            SOPL.accept("这里调用"+fitCoreBasicObserverInter.getClass().getSimpleName()+"方法或其他操作，可以传参等");
            fitCoreBasicObserverInter.fitCoreBasicObserver();
        }
        SOPL.accept("通知所有的被侦测观察的实体结束");
    }
}
