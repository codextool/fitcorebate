package top.topicsky.www.fitcore.system.kernel.engine.consumer;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;

import static top.topicsky.www.fitcore.global.resource.constant.eternalenum.FitCoreEternalEnumVariable.*;
import static top.topicsky.www.fitcore.system.kernel.common.consumer.ConsumerUtil.SOPLOUT;
import static top.topicsky.www.fitcore.system.kernel.common.consumer.ConsumerUtil.SOPLS;
import static top.topicsky.www.fitcore.system.kernel.common.function.FunctionUtil.OTS;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.system.kernel.engine.consumer
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 27 日 11 时 41 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Component
@Transactional
public interface FitCoreDafaultConsumerInter extends Serializable{
    /**
     * Init void dafault.
     *
     * @throws Exception
     *         the exception
     */
    default void initVoidDafault() throws Exception{
        SOPLS.accept(this.getClass().getPackage());
        SOPLOUT.accept(INITVOIDDAFAULT);
    }

    /**
     * Init basic dafault rt.
     *
     * @param <SI>
     *         the type parameter
     * @param <RT>
     *         the type parameter
     * @param si
     *         the si
     *
     * @return the rt
     *
     * @throws Exception
     *         the exception
     */
    default <SI,RT> RT initBasicDafault(SI si) throws Exception{
        SOPLS.accept(this.getClass().getPackage());
        SOPLOUT.accept(INITBASICDAFAULT);
        return (RT)si;
    }

    /**
     * Init no param dafault rt.
     *
     * @param <SI>
     *         the type parameter
     * @param <RT>
     *         the type parameter
     *
     * @return the rt
     *
     * @throws Exception
     *         the exception
     */
    default <SI,RT> RT initNoParamDafault() throws Exception{
        SOPLS.accept(this.getClass().getPackage());
        SOPLOUT.accept(INITNOPARAMDAFAULT);
        return (RT)OTS.apply(SUCCESS);
    }

    /**
     * Init no return dafault.
     *
     * @param <SI>
     *         the type parameter
     * @param si
     *         the si
     *
     * @throws Exception
     *         the exception
     */
    default <SI> void initNoReturnDafault(SI si) throws Exception{
        SOPLS.accept(this.getClass().getPackage());
        SOPLOUT.accept(INITNORETURNDAFAULT);
    }
}
