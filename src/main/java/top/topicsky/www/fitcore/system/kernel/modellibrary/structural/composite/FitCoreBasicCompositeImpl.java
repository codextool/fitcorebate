package top.topicsky.www.fitcore.system.kernel.modellibrary.structural.composite;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Enumeration;
import java.util.Vector;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.system.kernel.modellibrary.structural.composite
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 28 日 16 时 37 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Getter(AccessLevel.PUBLIC)
@Setter(AccessLevel.PUBLIC)
public class FitCoreBasicCompositeImpl implements Serializable{
    private String name;
    private FitCoreBasicCompositeImpl fitCoreBasicCompositeImpl;
    private Vector<FitCoreBasicCompositeImpl> children=new Vector<FitCoreBasicCompositeImpl>();

    /**
     * Instantiates a new Fit core basic composite.
     *
     * @param name
     *         the name
     */
    public FitCoreBasicCompositeImpl(String name){
        this.name=name;
    }

    /**
     * Add.
     *
     * @param node
     *         the node
     */
//添加孩子节点
    public void add(FitCoreBasicCompositeImpl node){
        children.add(node);
    }

    /**
     * Remove.
     *
     * @param node
     *         the node
     */
//删除孩子节点
    public void remove(FitCoreBasicCompositeImpl node){
        children.remove(node);
    }

    /**
     * Get children enumeration.
     *
     * @return the enumeration
     */
//取得孩子节点
    public Enumeration<FitCoreBasicCompositeImpl> getChildren(){
        return children.elements();
    }
}
