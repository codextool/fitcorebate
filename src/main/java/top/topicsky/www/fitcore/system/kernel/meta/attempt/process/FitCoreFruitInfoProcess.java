package top.topicsky.www.fitcore.system.kernel.meta.attempt.process;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import top.topicsky.www.fitcore.system.kernel.meta.attempt.core.FitCoreFruitColorMeta;
import top.topicsky.www.fitcore.system.kernel.meta.attempt.core.FitCoreFruitNameMeta;
import top.topicsky.www.fitcore.system.kernel.meta.attempt.core.FitCoreFruitProviderMeta;
import top.topicsky.www.fitcore.system.kernel.meta.attempt.model.FitCoreAppleImpl;

import java.io.Serializable;
import java.lang.reflect.Field;

import static top.topicsky.www.fitcore.system.kernel.common.function.FunctionUtil.OTS;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.system.kernel.meta.attempt
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 07 月 07 日 11 时 16 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Component
@Transactional
public class FitCoreFruitInfoProcess implements Serializable{
    /**
     * Get fruit info fit core apple.
     *
     * @param clazz
     *         the clazz
     *
     * @return the fit core apple
     */
    public static FitCoreAppleImpl getFruitInfo(Class<?> clazz){
        FitCoreAppleImpl fitCoreApple=new FitCoreAppleImpl();
        Field[] fields=clazz.getDeclaredFields();
        for(Field field : fields){
            FitCoreFruitNameMeta annotationFruitName=field.getAnnotation(FitCoreFruitNameMeta.class);
            FitCoreFruitColorMeta annotationFruitColor=field.getAnnotation(FitCoreFruitColorMeta.class);
            FitCoreFruitProviderMeta annotationFruitProvider=field.getAnnotation(FitCoreFruitProviderMeta.class);
            if(field.isAnnotationPresent(FitCoreFruitNameMeta.class)){
                fitCoreApple.setAppleName(annotationFruitName.value());
            }else if(field.isAnnotationPresent(FitCoreFruitColorMeta.class)){
                fitCoreApple.setAppleColor(OTS.apply(annotationFruitColor.fruitColor()));
            }else if(field.isAnnotationPresent(FitCoreFruitProviderMeta.class)){
                fitCoreApple.setAppleProvider("供应商编号["+
                        annotationFruitProvider.id()+
                        "]，供应商名称["+
                        annotationFruitProvider.name()+
                        "]，供应商地址["+
                        annotationFruitProvider.address()+
                        "]");
            }
        }
        return fitCoreApple;
    }

    /**
     * Gets fruit info object.
     *
     * @param object
     *         the object
     *
     * @return the fruit info object
     *
     * @throws Exception
     *         the exception
     */
    public static FitCoreAppleImpl getFruitInfoObject(Object object) throws Exception{
        Field[] fields=object.getClass().getDeclaredFields();
        FitCoreAppleImpl fitCoreApple=new FitCoreAppleImpl();
        for(Field field : fields){
            field.setAccessible(true);
            FitCoreFruitNameMeta annotationFruitName=field.getAnnotation(FitCoreFruitNameMeta.class);
            FitCoreFruitColorMeta annotationFruitColor=field.getAnnotation(FitCoreFruitColorMeta.class);
            FitCoreFruitProviderMeta annotationFruitProvider=field.getAnnotation(FitCoreFruitProviderMeta.class);
            Object filedstemp=field.get(object);
            if(field.isAnnotationPresent(FitCoreFruitNameMeta.class)){
                if(filedstemp!=null){
                    fitCoreApple.setAppleName(OTS.apply(filedstemp));
                }else{
                    fitCoreApple.setAppleName(annotationFruitName.value());
                }
            }else if(field.isAnnotationPresent(FitCoreFruitColorMeta.class)){
                if(filedstemp!=null){
                    fitCoreApple.setAppleColor(OTS.apply(filedstemp));
                }else{
                    fitCoreApple.setAppleColor(OTS.apply((annotationFruitColor).fruitColor()));
                }
            }else if(field.isAnnotationPresent(FitCoreFruitProviderMeta.class)){
                if(filedstemp!=null){
                    fitCoreApple.setAppleProvider(OTS.apply(filedstemp));
                }else{
                    fitCoreApple.setAppleProvider("供应商编号["+
                            annotationFruitProvider.id()+
                            "]，供应商名称["+
                            annotationFruitProvider.name()+
                            "]，供应商地址["+
                            annotationFruitProvider.address()+
                            "]");
                }
            }
        }
        return fitCoreApple;
    }
}
