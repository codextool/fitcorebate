package top.topicsky.www.fitcore.system.kernel.meta.original.xconstant.xpublic.model;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import top.topicsky.www.fitcore.system.kernel.meta.original.xconstant.xpublic.core.FCWeekPublic;

import java.io.Serializable;

import static top.topicsky.www.fitcore.global.resource.constant.eternalpublic.FitCoreEternalPublicWeek.*;

/**
 * The type Fc week model public.
 */
@Getter(AccessLevel.PUBLIC)
@Setter(AccessLevel.PUBLIC)
@Component
@Transactional
public class FCWeekModelPublic implements Serializable{
    @FCWeekPublic
    private String weekDafault;
    @FCWeekPublic(weekExpString=SUNDAY)
    private String weekSUNDAY;
    @FCWeekPublic(weekExpString=MONDAY)
    private String weekMONDAY;
    @FCWeekPublic(weekExpString=TUESDAY)
    private String weekTUESDAY;
    @FCWeekPublic(weekExpString=WEDNESDAY)
    private String weekWEDNESDAY;
    @FCWeekPublic(weekExpString=THURSDAY)
    private String weekTHURSDAY;
    @FCWeekPublic(weekExpString=FRIDAY)
    private String weekFRIDAY;
    @FCWeekPublic(weekExpString=SATURDAY)
    private String weekSATURDAY;
    @FCWeekPublic(weekExpString=SUNDAYC)
    private String weekSUNDAYC;
    @FCWeekPublic(weekExpString=MONDAYC)
    private String weekMONDAYC;
    @FCWeekPublic(weekExpString=TUESDAYC)
    private String weekTUESDAYC;
    @FCWeekPublic(weekExpString=WEDNESDAYC)
    private String weekWEDNESDAYC;
    @FCWeekPublic(weekExpString=THURSDAYC)
    private String weekTHURSDAYC;
    @FCWeekPublic(weekExpString=FRIDAYC)
    private String weekFRIDAYC;
    @FCWeekPublic(weekExpString=SATURDAYC)
    private String weekSATURDAYC;
    private String weekNoMetaSUNDAY;
    private String weekNoMetaMONDAY;
    private String weekNoMetaTUESDAY;
    private String weekNoMetaWEDNESDAY;
    private String weekNoMetaTHURSDAY;
    private String weekNoMetaFRIDAY;
    private String weekNoMetaSATURDAY;
    private String weekNoMetaSUNDAYC;
    private String weekNoMetaMONDAYC;
    private String weekNoMetaTUESDAYC;
    private String weekNoMetaWEDNESDAYC;
    private String weekNoMetaTHURSDAYC;
    private String weekNoMetaFRIDAYC;
    private String weekNoMetaSATURDAYC;

    @Override
    public String toString(){
        return "FCWeekModelPublic{"+
                "weekDafault='"+weekDafault+'\''+
                ", weekSUNDAY='"+weekSUNDAY+'\''+
                ", weekMONDAY='"+weekMONDAY+'\''+
                ", weekTUESDAY='"+weekTUESDAY+'\''+
                ", weekWEDNESDAY='"+weekWEDNESDAY+'\''+
                ", weekTHURSDAY='"+weekTHURSDAY+'\''+
                ", weekFRIDAY='"+weekFRIDAY+'\''+
                ", weekSATURDAY='"+weekSATURDAY+'\''+
                ", weekSUNDAYC='"+weekSUNDAYC+'\''+
                ", weekMONDAYC='"+weekMONDAYC+'\''+
                ", weekTUESDAYC='"+weekTUESDAYC+'\''+
                ", weekWEDNESDAYC='"+weekWEDNESDAYC+'\''+
                ", weekTHURSDAYC='"+weekTHURSDAYC+'\''+
                ", weekFRIDAYC='"+weekFRIDAYC+'\''+
                ", weekSATURDAYC='"+weekSATURDAYC+'\''+
                ", weekNoMetaSUNDAY='"+weekNoMetaSUNDAY+'\''+
                ", weekNoMetaMONDAY='"+weekNoMetaMONDAY+'\''+
                ", weekNoMetaTUESDAY='"+weekNoMetaTUESDAY+'\''+
                ", weekNoMetaWEDNESDAY='"+weekNoMetaWEDNESDAY+'\''+
                ", weekNoMetaTHURSDAY='"+weekNoMetaTHURSDAY+'\''+
                ", weekNoMetaFRIDAY='"+weekNoMetaFRIDAY+'\''+
                ", weekNoMetaSATURDAY='"+weekNoMetaSATURDAY+'\''+
                ", weekNoMetaSUNDAYC='"+weekNoMetaSUNDAYC+'\''+
                ", weekNoMetaMONDAYC='"+weekNoMetaMONDAYC+'\''+
                ", weekNoMetaTUESDAYC='"+weekNoMetaTUESDAYC+'\''+
                ", weekNoMetaWEDNESDAYC='"+weekNoMetaWEDNESDAYC+'\''+
                ", weekNoMetaTHURSDAYC='"+weekNoMetaTHURSDAYC+'\''+
                ", weekNoMetaFRIDAYC='"+weekNoMetaFRIDAYC+'\''+
                ", weekNoMetaSATURDAYC='"+weekNoMetaSATURDAYC+'\''+
                '}';
    }
}
