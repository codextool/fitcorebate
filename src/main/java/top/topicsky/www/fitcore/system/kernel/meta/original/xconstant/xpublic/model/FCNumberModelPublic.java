package top.topicsky.www.fitcore.system.kernel.meta.original.xconstant.xpublic.model;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import top.topicsky.www.fitcore.system.kernel.meta.original.xconstant.xpublic.core.FCNumberPublic;

import java.io.Serializable;

import static top.topicsky.www.fitcore.global.resource.constant.eternalpublic.FitCoreEternalPublicNumber.*;

/**
 * The type Fc number model public.
 */
@Getter(AccessLevel.PUBLIC)
@Setter(AccessLevel.PUBLIC)
@Component
@Transactional
public class FCNumberModelPublic implements Serializable{
    @FCNumberPublic
    private String numberDafault;
    @FCNumberPublic(numberExp=FITZERO)
    private String numberFITZERO;
    @FCNumberPublic(numberExp=FITONE)
    private String numberFITONE;
    @FCNumberPublic(numberExp=FITTWO)
    private String numberFITTWO;
    @FCNumberPublic(numberExp=FITTHREE)
    private String numberFITTHREE;
    @FCNumberPublic(numberExp=FITFOUR)
    private String numberFITFOUR;
    @FCNumberPublic(numberExp=FITFIVE)
    private String numberFITFIVE;
    @FCNumberPublic(numberExp=FITSIX)
    private String numberFITSIX;
    @FCNumberPublic(numberExp=FITSEVEN)
    private String numberFITSEVEN;
    @FCNumberPublic(numberExp=FITEIGHT)
    private String numberFITEIGHT;
    @FCNumberPublic(numberExp=FITNINE)
    private String numberFITNINE;
    @FCNumberPublic(numberExp=FITZEROC)
    private String numberFITZEROC;
    @FCNumberPublic(numberExp=FITONEC)
    private String numberFITONEC;
    @FCNumberPublic(numberExp=FITTWOC)
    private String numberFITTWOC;
    @FCNumberPublic(numberExp=FITTHREEC)
    private String numberFITTHREEC;
    @FCNumberPublic(numberExp=FITFOURC)
    private String numberFITFOURC;
    @FCNumberPublic(numberExp=FITFIVEC)
    private String numberFITFIVEC;
    @FCNumberPublic(numberExp=FITSIXC)
    private String numberFITSIXC;
    @FCNumberPublic(numberExp=FITSEVENC)
    private String numberFITSEVENC;
    @FCNumberPublic(numberExp=FITEIGHTC)
    private String numberFITEIGHTC;
    @FCNumberPublic(numberExp=FITNINEC)
    private String numberFITNINEC;
    private String numberNoMetaFITZERO;
    private String numberNoMetaFITONE;
    private String numberNoMetaFITTWO;
    private String numberNoMetaFITTHREE;
    private String numberNoMetaFITFOUR;
    private String numberNoMetaFITFIVE;
    private String numberNoMetaFITSIX;
    private String numberNoMetaFITSEVEN;
    private String numberNoMetaFITEIGHT;
    private String numberNoMetaFITNINE;
    private String numberNoMetaFITZEROC;
    private String numberNoMetaFITONEC;
    private String numberNoMetaFITTWOC;
    private String numberNoMetaFITTHREEC;
    private String numberNoMetaFITFOURC;
    private String numberNoMetaFITFIVEC;
    private String numberNoMetaFITSIXC;
    private String numberNoMetaFITSEVENC;
    private String numberNoMetaFITEIGHTC;
    private String numberNoMetaFITNINEC;

    @Override
    public String toString(){
        return "FCNumberModelPublic{"+
                "numberDafault='"+numberDafault+'\''+
                ", numberFITZERO='"+numberFITZERO+'\''+
                ", numberFITONE='"+numberFITONE+'\''+
                ", numberFITTWO='"+numberFITTWO+'\''+
                ", numberFITTHREE='"+numberFITTHREE+'\''+
                ", numberFITFOUR='"+numberFITFOUR+'\''+
                ", numberFITFIVE='"+numberFITFIVE+'\''+
                ", numberFITSIX='"+numberFITSIX+'\''+
                ", numberFITSEVEN='"+numberFITSEVEN+'\''+
                ", numberFITEIGHT='"+numberFITEIGHT+'\''+
                ", numberFITNINE='"+numberFITNINE+'\''+
                ", numberFITZEROC='"+numberFITZEROC+'\''+
                ", numberFITONEC='"+numberFITONEC+'\''+
                ", numberFITTWOC='"+numberFITTWOC+'\''+
                ", numberFITTHREEC='"+numberFITTHREEC+'\''+
                ", numberFITFOURC='"+numberFITFOURC+'\''+
                ", numberFITFIVEC='"+numberFITFIVEC+'\''+
                ", numberFITSIXC='"+numberFITSIXC+'\''+
                ", numberFITSEVENC='"+numberFITSEVENC+'\''+
                ", numberFITEIGHTC='"+numberFITEIGHTC+'\''+
                ", numberFITNINEC='"+numberFITNINEC+'\''+
                ", numberNoMetaFITZERO='"+numberNoMetaFITZERO+'\''+
                ", numberNoMetaFITONE='"+numberNoMetaFITONE+'\''+
                ", numberNoMetaFITTWO='"+numberNoMetaFITTWO+'\''+
                ", numberNoMetaFITTHREE='"+numberNoMetaFITTHREE+'\''+
                ", numberNoMetaFITFOUR='"+numberNoMetaFITFOUR+'\''+
                ", numberNoMetaFITFIVE='"+numberNoMetaFITFIVE+'\''+
                ", numberNoMetaFITSIX='"+numberNoMetaFITSIX+'\''+
                ", numberNoMetaFITSEVEN='"+numberNoMetaFITSEVEN+'\''+
                ", numberNoMetaFITEIGHT='"+numberNoMetaFITEIGHT+'\''+
                ", numberNoMetaFITNINE='"+numberNoMetaFITNINE+'\''+
                ", numberNoMetaFITZEROC='"+numberNoMetaFITZEROC+'\''+
                ", numberNoMetaFITONEC='"+numberNoMetaFITONEC+'\''+
                ", numberNoMetaFITTWOC='"+numberNoMetaFITTWOC+'\''+
                ", numberNoMetaFITTHREEC='"+numberNoMetaFITTHREEC+'\''+
                ", numberNoMetaFITFOURC='"+numberNoMetaFITFOURC+'\''+
                ", numberNoMetaFITFIVEC='"+numberNoMetaFITFIVEC+'\''+
                ", numberNoMetaFITSIXC='"+numberNoMetaFITSIXC+'\''+
                ", numberNoMetaFITSEVENC='"+numberNoMetaFITSEVENC+'\''+
                ", numberNoMetaFITEIGHTC='"+numberNoMetaFITEIGHTC+'\''+
                ", numberNoMetaFITNINEC='"+numberNoMetaFITNINEC+'\''+
                '}';
    }
}
