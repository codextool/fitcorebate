package top.topicsky.www.fitcore.system.kernel.modellibrary.behavioral.inherit.templatemethod;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.system.kernel.modellibrary.behavioral.inherit.templatemethod
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 29 日 13 时 28 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Component
@Transactional
public abstract class FitCoreBasicTemplatementhodAbstractImpl implements Serializable{
    /**
     * Fit core basic templatementhod abstract inherit double.
     *
     * @param exp
     *         the exp
     * @param opt
     *         the opt
     *
     * @return the double
     */
/*主方法，实现对本类其它方法的调用*/
    public final Double fitCoreBasicTemplatementhodAbstractInherit(String exp,String opt){
        Double[] array=split(exp,opt);
        return fitCoreBasicTemplatementhodAbstract(array[0],array[1]);
    }

    /**
     * Fit core basic templatementhod abstract double.
     *
     * @param num1
     *         the num 1
     * @param num2
     *         the num 2
     *
     * @return the double
     */
/*被子类重写的方法*/
    abstract public Double fitCoreBasicTemplatementhodAbstract(Double num1,Double num2);

    /**
     * Split double [ ].
     *
     * @param exp
     *         the exp
     * @param opt
     *         the opt
     *
     * @return the double [ ]
     */
    public Double[] split(String exp,String opt){
        String array[]=exp.split(opt);
        Double arrayDouble[]=new Double[2];
        arrayDouble[0]=Double.parseDouble(array[0]);
        arrayDouble[1]=Double.parseDouble(array[1]);
        return arrayDouble;
    }
}
