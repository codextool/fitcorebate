package top.topicsky.www.fitcore.system.kernel.meta.original.xconstant.xpublic.core;

import java.lang.annotation.*;

import static top.topicsky.www.fitcore.global.resource.constant.eternalpublic.FitCoreEternalPublicNumber.FITZERO;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.system.kernel.meta.original.xconstant.xpublic.core
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 07 月 07 日 15 时 20 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface FCNumberPublic{
    /**
     * Assert exp string.
     *
     * @return the string
     */
    String numberExp() default FITZERO;
}
