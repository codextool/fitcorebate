package top.topicsky.www.fitcore.system.kernel.modellibrary.structural.facade;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;

import static top.topicsky.www.fitcore.system.kernel.common.consumer.ConsumerUtil.SOPL;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.system.kernel.modellibrary.structural.facade
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 28 日 13 时 44 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Component
@Transactional
public class FitCoreBasicFacadeCoreImpl implements Serializable{
    /**
     * Init facade.
     */
    public void initFacade(){
        SOPL.accept("这里将启动初始化核心（Core）业务模块");
    }

    /**
     * Halt facade.
     */
    public void haltFacade(){
        SOPL.accept("这里将结束并关闭核心(Core)业务模块");
    }
}
