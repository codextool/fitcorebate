package top.topicsky.www.fitcore.system.kernel.modellibrary.behavioral.level.iterator;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.system.kernel.modellibrary.behavioral.level.iterator
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 29 日 14 时 55 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Component
@Transactional
public interface FitCoreBasicIteratorCollectionIteratorInter extends Serializable{
    /**
     * Obtain previous object.
     *
     * @return the object
     */
//取得前一个游标
    public Object obtainPrevious();

    /**
     * Obtain next object.
     *
     * @return the object
     */
//取得后一个游标
    public Object obtainNext();

    /**
     * Has next boolean.
     *
     * @return the boolean
     */
    public boolean hasNext();

    /**
     * Obtain first object.
     *
     * @return the object
     */
//取得第一个游标
    public Object obtainFirst();
}
