package top.topicsky.www.fitcore.system.kernel.modellibrary.behavioral.level.command;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;

import static top.topicsky.www.fitcore.system.kernel.common.consumer.ConsumerUtil.SOPL;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.system.kernel.modellibrary.behavioral.level.command
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 29 日 17 时 05 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Component
@Transactional
public class FitCoreBasicInvokerImpl implements Serializable{
    private FitCoreBasicCommandInter fitCoreBasicCommandInter;

    /**
     * Instantiates a new Fit core basic invoker.
     *
     * @param fitCoreBasicCommandInter
     *         the fit core basic command inter
     */
    public FitCoreBasicInvokerImpl(FitCoreBasicCommandInter fitCoreBasicCommandInter){
        this.fitCoreBasicCommandInter=fitCoreBasicCommandInter;
    }

    /**
     * Fit core basic invoker.
     */
    public void FitCoreBasicInvoker(){
        SOPL.accept("这里是【司令部】，向【执行机构】发布XXX命令，由【秘书】转发命令——发布");
        fitCoreBasicCommandInter.fitCoreBasicCommand();
        SOPL.accept("这里是【司令部】，向【执行机构】发布XXX命令，由【秘书】转发命令——回执");
    }
}
