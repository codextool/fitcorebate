package top.topicsky.www.fitcore.system.kernel.meta.original.xconstant.xpublic.process;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import top.topicsky.www.fitcore.system.kernel.meta.original.xconstant.xpublic.core.FCAssertPublic;
import top.topicsky.www.fitcore.system.kernel.meta.original.xconstant.xpublic.model.FCAssertModelPublic;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.function.Supplier;

import static top.topicsky.www.fitcore.system.kernel.common.function.FunctionUtil.OTS;
import static top.topicsky.www.fitcore.system.kernel.common.function.FunctionUtil.STCHAR;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.system.kernel.meta.original.xconstant.xpublic.process
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 07 月 07 日 15 时 32 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Component
@Transactional
public class FCAssertProcessPublic implements Serializable{
    /**
     * The Fc assert model public golbal.
     */
    static Supplier<FCAssertModelPublic> fcAssertModelPublicGolbal=()->new FCAssertModelPublic();
    /**
     * The Clazz fc assert public.
     */
    static Class<FCAssertPublic> clazzFCAssertPublic=FCAssertPublic.class;

    /**
     * Get fruit info fit core apple.
     *
     * @param clazz
     *         the clazz
     *
     * @return the fit core apple
     *
     * @throws NoSuchMethodException
     *         the no such method exception
     * @throws InvocationTargetException
     *         the invocation target exception
     * @throws IllegalAccessException
     *         the illegal access exception
     */
    public static FCAssertModelPublic getFCAssertPublicInfo(Class<?> clazz) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException{
        FCAssertModelPublic fcAssertModelPublic=fcAssertModelPublicGolbal.get();
        Field[] fields=clazz.getDeclaredFields();
        for(Field field : fields){
            FCAssertPublic annotationFCAssertPublicField=field.getAnnotation(clazzFCAssertPublic);
            boolean annotationFCAssertPublicBoolean=field.isAnnotationPresent(clazzFCAssertPublic);
            Class fcAssertModelPublicClazz=fcAssertModelPublic.getClass();
            Method m1=fcAssertModelPublicClazz.getDeclaredMethod("set"+String.valueOf(STCHAR.apply(field.getName())),String.class);
            if(annotationFCAssertPublicBoolean){
                m1.invoke(fcAssertModelPublic,annotationFCAssertPublicField.assertExp());
            }
        }
        return fcAssertModelPublic;
    }

    /**
     * Gets fruit info object.
     *
     * @param object
     *         the object
     *
     * @return the fruit info object
     *
     * @throws Exception
     *         the exception
     */
    public static FCAssertModelPublic getFCAssertPublicInfoObject(Object object) throws Exception{
        FCAssertModelPublic fcAssertModelPublic=fcAssertModelPublicGolbal.get();
        Field[] fields=object.getClass().getDeclaredFields();
        for(Field field : fields){
            field.setAccessible(true);
            Object filedstemp=field.get(object);
            FCAssertPublic annotationFCAssertPublicField=field.getAnnotation(clazzFCAssertPublic);
            boolean annotationFCAssertPublicBoolean=field.isAnnotationPresent(clazzFCAssertPublic);
            Class fcAssertModelPublicClazz=fcAssertModelPublic.getClass();
            Method m1=fcAssertModelPublicClazz.getDeclaredMethod("set"+String.valueOf(STCHAR.apply(field.getName())),String.class);
            if(annotationFCAssertPublicBoolean){
                if(filedstemp!=null){
                    m1.invoke(fcAssertModelPublic,OTS.apply(filedstemp));
                }else{
                    m1.invoke(fcAssertModelPublic,annotationFCAssertPublicField.assertExp());
                }
            }else{
                if(filedstemp!=null){
                    m1.invoke(fcAssertModelPublic,OTS.apply(filedstemp));
                }
            }
        }
        return fcAssertModelPublic;
    }
}
