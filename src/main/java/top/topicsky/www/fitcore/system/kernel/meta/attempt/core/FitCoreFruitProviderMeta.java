package top.topicsky.www.fitcore.system.kernel.meta.attempt.core;

import java.lang.annotation.*;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.system.kernel.meta.attempt
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 07 月 07 日 11 时 14 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface FitCoreFruitProviderMeta{
    /**
     * 供应商编号
     *
     * @return the int
     */
    public int id() default -1;

    /**
     * 供应商名称
     *
     * @return the string
     */
    public String name() default "";

    /**
     * 供应商地址
     *
     * @return the string
     */
    public String address() default "";
}
