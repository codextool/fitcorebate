package top.topicsky.www.fitcore.system.kernel.meta.original.xconstant.xpublic.model;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import top.topicsky.www.fitcore.system.kernel.meta.original.xconstant.xpublic.core.FCLayoutPublic;

import java.io.Serializable;

import static top.topicsky.www.fitcore.global.resource.constant.eternalpublic.FitCoreEternalPublicLayout.ERRORJSP;
import static top.topicsky.www.fitcore.global.resource.constant.eternalpublic.FitCoreEternalPublicLayout.INDEXJSP;

/**
 * The type Fc layout model public.
 */
@Getter(AccessLevel.PUBLIC)
@Setter(AccessLevel.PUBLIC)
@Component
@Transactional
public class FCLayoutModelPublic implements Serializable{
    @FCLayoutPublic
    private String layoutLOGINJSP;
    @FCLayoutPublic(layoutExp=INDEXJSP)
    private String layoutINDEXJSP;
    @FCLayoutPublic(layoutExp=ERRORJSP)
    private String layoutERRORJSP;
    private String layoutNoMetaINDEXJSP;
    private String layoutNoMetaERRORJSP;

    @Override
    public String toString(){
        return "FCLayoutModelPublic{"+
                "layoutLOGINJSP='"+layoutLOGINJSP+'\''+
                ", layoutINDEXJSP='"+layoutINDEXJSP+'\''+
                ", layoutERRORJSP='"+layoutERRORJSP+'\''+
                ", layoutNoMetaINDEXJSP='"+layoutNoMetaINDEXJSP+'\''+
                ", layoutNoMetaERRORJSP='"+layoutNoMetaERRORJSP+'\''+
                '}';
    }
}
