package top.topicsky.www.fitcore.system.kernel.modellibrary.behavioral.level.responsibilitychain;

import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.system.kernel.modellibrary.behavioral.level.responsibilitychain
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 29 日 16 时 35 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Transactional
public class FitCoreBasicChainAbstractBeanImpl extends FitCoreBasicChainAbstractImpl implements FitCoreBasicChainInter, Serializable{
    private String name;

    /**
     * Instantiates a new Fit core basic chain abstract bean.
     *
     * @param name
     *         the name
     */
    public FitCoreBasicChainAbstractBeanImpl(String name){
        this.name=name;
    }

    @Override
    public void fitCoreBasicChain(){
        System.out.println(name+"吃完了");
        if(getFitCoreBasicChainInter()!=null){
            getFitCoreBasicChainInter().fitCoreBasicChain();
        }
    }
}
